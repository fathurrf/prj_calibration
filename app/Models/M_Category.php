<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Category extends Model
{
    protected $table = 'tb_category';
    protected $primaryKey = 'id';
}
