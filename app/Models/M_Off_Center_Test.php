<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Off_Center_Test extends Model
{
    protected $table = 'tb_off_center_test';
    protected $primaryKey = 'id';

    public function getCondition()
    {
        return $this->belongsTo(M_Condition::class, 'id_condition','id');
    }

    public function getDetailOct()
    {
        return $this->hasMany(M_Detail_Oct::class, 'id_oct','id');
    }
}
