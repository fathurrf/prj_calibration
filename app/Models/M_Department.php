<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Department extends Model
{
    protected $table = 'tb_department';
    protected $primaryKey = 'id';
}
