<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Standard_Certificate extends Model
{
    protected $table = 'tb_standard_certificate';
    protected $primaryKey = 'id';
}
