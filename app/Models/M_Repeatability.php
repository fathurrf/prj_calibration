<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Repeatability extends Model
{
    protected $table = 'tb_repeatability';
    protected $primaryKey = 'id';
}
