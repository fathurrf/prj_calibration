<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Readability extends Model
{
    protected $table = 'tb_readability';
    protected $primaryKey = 'id';
}
