<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Detail_Rt extends Model
{
    protected $table = 'tb_detail_rt';
    protected $primaryKey = 'id';

    public function getRt()
    {
        return $this->belongsTo(M_Repeatability_Test::class, 'id_rt','id');
    }

    public function getCap()
    {
        return $this->belongsTo(M_Cap::class, 'id_cap','id');
    }
}
