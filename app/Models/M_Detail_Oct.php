<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Detail_Oct extends Model
{
    protected $table = 'tb_detail_oct';
    protected $primaryKey = 'id';

    public function getOct()
    {
        return $this->belongsTo(M_Off_Center_Test::class, 'id_oct','id');
    }
    
    public function getPosition()
    {
        return $this->belongsTo(M_Position::class, 'id_position','id');
    }
}
