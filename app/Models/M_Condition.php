<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class M_Condition extends Model
{
    protected $table = 'tb_condition';
    protected $primaryKey = 'id';

    public function getInstrument()
    {
        return $this->belongsTo(M_Instrument::class, 'id_instrument','id');
    }

    public function getOct()
    {
        return $this->belongsTo(M_Off_Center_Test::class, 'id','id_condition');
    }

    public function getRt()
    {
        return $this->belongsTo(M_Repeatability_Test::class, 'id','id_condition');
    }

    public function getUserPrepare()
    {
        return $this->belongsTo(User::class, 'id_user_prepare','id');
    }

    public function getUserApprove()
    {
        return $this->belongsTo(User::class, 'id_user_approve','id');
    }
}
