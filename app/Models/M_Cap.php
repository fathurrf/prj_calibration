<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Cap extends Model
{
    protected $table = 'tb_cap';
    protected $primaryKey = 'id';
}
