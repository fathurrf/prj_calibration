<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Repeatability_Test extends Model
{
    protected $table = 'tb_repeatability_test';
    protected $primaryKey = 'id';

    public function getCondition()
    {
        return $this->belongsTo(M_Condition::class, 'id_condition','id');
    }
    
    public function getDetailRt()
    {
        return $this->hasMany(M_Detail_Rt::class, 'id_rt','id');
    }
}
