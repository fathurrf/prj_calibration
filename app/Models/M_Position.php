<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Position extends Model
{
    protected $table = 'tb_position';
    protected $primaryKey = 'id';
}
