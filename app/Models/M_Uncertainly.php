<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Uncertainly extends Model
{
    protected $table = 'tb_uncertainly';
    protected $primaryKey = 'id';
}
