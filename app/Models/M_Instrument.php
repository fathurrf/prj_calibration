<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Instrument extends Model
{
    protected $table = 'tb_instrument';
    protected $primaryKey = 'id';

    public function getCategory()
    {
        return $this->belongsTo(M_Category::class, 'id_category','id');
    }
}
