<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class M_Distribusi extends Model
{
    protected $table = 'tb_distribusi';
    protected $primaryKey = 'id';
}
