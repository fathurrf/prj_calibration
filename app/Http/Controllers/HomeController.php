<?php

namespace App\Http\Controllers;

use App\Models\M_Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboard()
    {
        $data = M_Category::all();
        return view('dashboard',compact('data'));
    }
}
