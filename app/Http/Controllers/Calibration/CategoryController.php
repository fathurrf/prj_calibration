<?php

namespace App\Http\Controllers\Calibration;

use App\Http\Controllers\Controller;
use App\Models\M_Category;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class CategoryController extends Controller
{
    public function index()
    {
        $data = M_Category::all();
        return view('Category.Category', compact('data'));
    }

    public function show()
    {
        $data = M_Category::all();
        return DataTables::of($data)
        ->addColumn('action', function($data){
            $save_method = 'edit';
            return 
            '<a onclick="add_category('."'$data->id'".','."'$save_method'".')" id="edit_category" data-toggle="modal" data-target="#modal_add_category" class="waves-effect" title="Edit Category"><i class="material-icons" style="color:yellow">edit</i></a>&nbsp;'.
            '<a onclick="delete_category('."'$data->id'".')" id="delete_category" class="waves-effect" title="Delete Category"><i class="material-icons" style="color:red">delete</i></a>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function post(Request $request){
        // return $request;
        $id_user = Auth::user()->id;
        if($request->input('id')){
            $update = M_Category::where('id', $request->id)
            ->update([
                'category_name' => $request->category_name,
                'id_user_edit'=> $id_user,
            ]);
            // $id_user = $update->id;
            if($update){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil diupdate',
                    // 'id_user'   => $id_user
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal diupdate'
                ]);
            }
        }else{
            $simpan = new M_Category();
            $simpan->category_name  = $request->category_name;
            $simpan->id_user = $id_user;
            $simpan->save();
            $id_category = $simpan->id;
            if($id_category){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil disimpan',
                    'id_category'   => $id_category
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal disimpan'
                ]);
            }
        }
    }

    public function showEdit(Request $request)
    {
        // return $request;
        $data = M_Category::where('id',$request->id)->first();
        return response()->json($data);
    }
    
    public function delete(Request $request)
    {
        // return $request;
        $data = M_Category::where('id',$request->id)->delete();

        return response()->json([
            'code'      => 200,
            'status'    => 'success',
            'message'   => 'Data berhasil dihapus',
            'data'      => $data
        ]);
    }
}
