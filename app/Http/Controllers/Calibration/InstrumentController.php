<?php

namespace App\Http\Controllers\Calibration;

use App\Http\Controllers\Controller;
use App\Models\M_Category;
use App\Models\M_Condition;
use App\Models\M_Instrument;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class InstrumentController extends Controller
{
    public function index(Request $request)
    {
        // $id = $request->id; 
        $data = M_Category::all();
        return view('Instrument.Instrument',compact('data'));
    }

    public function show()
    {
        $data = M_Instrument::all();
        return DataTables::of($data)
        ->addColumn('action', function($data){
            $save_method = 'edit';
            return 
            '<a onclick="add_instrument('."'$data->id'".','."'$save_method'".')" id="edit_instrument" data-toggle="modal" data-target="#modal_add_instrument" class="waves-effect" title="Edit Instrument"><i class="material-icons" style="color:yellow">edit</i></a>&nbsp;'.
            '<a onclick="delete_instrument('."'$data->id'".')" id="delete_instrument" class="waves-effect" title="Delete Instrument"><i class="material-icons" style="color:red">delete</i></a>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function post(Request $request){
        // return $request;
        $id_user = Auth::user()->id;
        if($request->input('id')){
            $update = M_Instrument::where('id', $request->id)
            ->update([
                'instrument_name' => $request->instrument_name,
                'id_category' => $request->select_category,
                'manufacturer'=> $request->manufacturer,
                'model_no'  => $request->model_no,
                'serial_no'  => $request->serial_no,
                'max_capacity' => $request->max_capacity,
                'resolution'=> $request->resolution,
                'type'=> $request->type,
                'id_user_edit'=> $id_user,
            ]);
            // $id_user = $update->id;
            if($update){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil diupdate',
                    // 'id_user'   => $id_user
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal diupdate'
                ]);
            }
        }else{
            $simpan = new M_Instrument();
            $simpan->instrument_name  = $request->instrument_name;
            $simpan->id_category = $request->select_category;
            $simpan->manufacturer = $request->manufacturer;
            $simpan->model_no   = $request->model_no;
            $simpan->serial_no   = $request->serial_no;
            $simpan->max_capacity  = $request->max_capacity;
            $simpan->resolution = $request->resolution;
            $simpan->type = $request->type;
            $simpan->id_user = $id_user;
            $simpan->save();
            $id_instrument = $simpan->id;
            if($id_instrument){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil disimpan',
                    'id_instrument'   => $id_instrument
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal disimpan'
                ]);
            }
        }
    }

    public function showEdit(Request $request)
    {
        // return $request;
        $data = M_Instrument::where('id',$request->id)->first();
        return response()->json($data);
    }
    
    public function delete(Request $request)
    {
        // return $request;
        $data = M_Instrument::where('id',$request->id)->delete();

        return response()->json([
            'code'      => 200,
            'status'    => 'success',
            'message'   => 'Data berhasil dihapus',
            'data'      => $data
        ]);
    }

    public function getCategory()
    {
        $data = M_Category::all();
        return response()->json($data);
    }
}
