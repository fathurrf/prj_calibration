<?php

namespace App\Http\Controllers\Calibration;

use App\Http\Controllers\Controller;
use App\Models\M_Category;
use Illuminate\Http\Request;

class RepeatabilityTestController extends Controller
{
    public function index(Request $request){
        $id = $request->id;
        $data = M_Category::all();
        $dataperid = M_Category::where('id',$id)->first();
        return view('RepeatabilityTest.RepeatabilityTest', compact('data','id','dataperid'));
    }
}
