<?php

namespace App\Http\Controllers\Calibration;

use App\Http\Controllers\Controller;
use App\Models\M_Category;
use App\Models\M_Condition;
use App\Models\M_Instrument;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class DetailInstrumentController extends Controller
{
    public function index(Request $request)
    {
        $id = $request->id; 
        $data = M_Category::all();
        $dataperid = M_Category::where('id',$id)->first();
        return view('Instrument.DetailInstrument', compact('data','id','dataperid'));
    }

    public function show(Request $request)
    {
        // return $request;
        $data = M_Instrument::where('id_category',$request->id)->get();
        return DataTables::of($data)
        ->addColumn('action', function($data){
            $save_method = 'edit';
            if(Auth::user()->role == 'operator'){
                return 
                '<a onclick="add_condition('."'$data->id'".','."'$data->instrument_name'".','."'add'".')" id="add_condition" data-toggle="modal" data-target="#modal_add_condition" class="waves-effect" title="Add Condition"><i class="material-icons" style="color:green">add_circle_outline</i></a>&nbsp;';
            }elseif(Auth::user()->role == 'manager'){
                return 
                '<a onclick="view_condition('."'$data->id'".','."'$data->instrument_name'".')" id="add_condition" data-toggle="modal" data-target="#modal_add_condition" class="waves-effect" title="Add Condition"><i class="material-icons" style="color:blue">visibility</i></a>&nbsp;';
            }else{
                return 
                '<a onclick="add_condition('."'$data->id'".','."'$data->instrument_name'".','."'add'".')" id="add_condition" data-toggle="modal" data-target="#modal_add_condition" class="waves-effect" title="Add Condition"><i class="material-icons" style="color:green">add_circle_outline</i></a>&nbsp;'.
                '<a onclick="view_condition('."'$data->id'".','."'$data->instrument_name'".')" id="add_condition" data-toggle="modal" data-target="#modal_add_condition" class="waves-effect" title="Add Condition"><i class="material-icons" style="color:blue">visibility</i></a>&nbsp;';
            }

        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function postCondition(Request $request)
    {
        // return $request;
        $id_user = Auth::user()->id;
        if($request->input('id_cond')){
            $update = M_Condition::
            where('id',$request->id_cond)
            ->update([
                // 'id_instrument' => $request->id,
                'location' => $request->location,
                'temp' => $request->temp,
                'humidity' => $request->humidity,
                'id_user_edit' => $id_user,
            ]);
            if($update){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil diupdate',
                    // 'id_user'   => $id_user
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal diupdate'
                ]);
            }
        }else{
            $simpan = new M_Condition();
            $simpan->id_instrument = $request->id;
            $simpan->location = $request->location;
            $simpan->temp = $request->temp;
            $simpan->humidity = $request->humidity;
            $simpan->id_user = $id_user;
            $simpan->status = 'open';
            $simpan->save();
            $id_condition = $simpan->id;
            if($id_condition){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil disimpan',
                    'id_condition'   => $id_condition
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal disimpan'
                ]);
            }
        }
    }

    public function showCondition(Request $request)
    {
        $data = M_Condition::where('id_instrument',$request->id)->with('getInstrument')->get();
        return DataTables::of($data)
        ->addColumn('calibration_date', function($data){
            return $calibration_date = $data->created_at->format('d M Y');
        })
        ->addColumn('input_hours', function($data){
            return $input_hours = $data->created_at->format('H:i');
        })
        ->addColumn('action', function($data){
            $save_method = 'edit';
            $instrument_name = $data->getInstrument->instrument_name;
            return 
            '<a onclick="add_condition('."'$data->id_instrument'".','."'$instrument_name'".','."'$save_method'".','."'$data->id'".')" id="edit_condition" data-toggle="modal" class="waves-effect" title="Edit Condition"><i class="material-icons" style="color:yellow">edit</i></a>&nbsp;'.
            '<a onclick="delete_condition('."'$data->id'".')" id="delete_condition" class="waves-effect" title="Delete Condition"><i class="material-icons" style="color:red">delete</i></a>';
        })
        ->rawColumns(['action','calibration_date','input_hours'])
        ->make(true);
    }

    public function detailCondition(Request $request)
    {
        // return $request;
        $data = M_Condition::where('id', $request->id)->first();
        return response()->json($data);

    }

    public function deleteCondition(Request $request)
    {
        // return $request;
        $data = M_Condition::where('id',$request->id)->delete();

        return response()->json([
            'code'      => 200,
            'status'    => 'success',
            'message'   => 'Data berhasil dihapus',
            'data'      => $data
        ]);
    }
}
