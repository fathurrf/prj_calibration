<?php

namespace App\Http\Controllers\Calibration;

use App\Http\Controllers\Controller;
use App\Models\M_Category;
use App\Models\M_Department;
use Illuminate\Http\Request;
use App\User;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        $data = M_Category::all();
        return view('UserManagement.UserManagement',compact('data'));
    }

    public function show()
    {
        $data = User::with('getDepartment')->get();
        return DataTables::of($data)
        // ->addColumn('department_name', function($data){
        //     return $data->get_department;
        //     //  $department_name;
        // })
        ->addColumn('action', function($data){
            $save_method = 'edit';
            return 
            '<a id="detail_user" class="btn btn-xs"><i class="material-icons">remove_red_eye</i></a>&nbsp;'.
            '<a onclick="add_user('."'$data->id'".','."'$save_method'".')" id="edit_user" data-toggle="modal" data-target="#modal_add_user" href="#" class="btn btn-xs"><i class="material-icons">edit</i></a>&nbsp;'.
            '<a onclick="delete_user('."'$data->id'".')" id="delete_user" href="#" class="btn btn-xs"><i class="material-icons">delete</i></a>';
        })
        ->rawColumns(['action','department_name'])
        ->make(true);
    }

    public function postUser(Request $request){
        // return $request;
        if($request->input('id')){
            $update = User::where('id', $request->id)
            ->update([
                'name'      => $request->nama,
                'email'     => $request->email,
                'username'  => $request->username,
                'password'  => bcrypt($request->password),
                'role'      => $request->select_role,
                'id_department'=> $request->select_department,
            ]);
            // $id_user = $update->id;
            if($update){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil diupdate',
                    // 'id_user'   => $id_user
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal diupdate'
                ]);
            }
        }else{
            $simpan = new User();
            $simpan->name       = $request->nama;
            $simpan->email      = $request->email;
            $simpan->username   = $request->username;
            $simpan->password   = bcrypt($request->password);
            $simpan->role       = $request->select_role;
            $simpan->id_department = $request->select_department;
            $simpan->save();
            $id_user = $simpan->id;
            if($id_user){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil disimpan',
                    'id_user'   => $id_user
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal disimpan'
                ]);
            }
        }
    }

    public function showEdit(Request $request)
    {
        // return $request;
        $data = User::where('id',$request->id)->first();
        return response()->json($data);
    }
    
    public function deleteUser(Request $request)
    {
        // return $request;
        $data = User::where('id',$request->id)->delete();

        return response()->json([
            'code'      => 200,
            'status'    => 'success',
            'message'   => 'Data berhasil dihapus',
            'data'      => $data
        ]);
    }

    public function getDepartment()
    {
        $data = M_Department::all();
        return response()->json($data);
    }
}
