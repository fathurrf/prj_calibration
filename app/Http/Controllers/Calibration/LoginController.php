<?php

namespace App\Http\Controllers\Calibration;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Routing\Redirector;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/';

    public function getLogin(){
        return view('login');
    }

    public function notfound(){
        return view('notfound');
    }

    public function postLogin(Request $request)
    {
        // return $request;
        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){
            // return $role= Auth::user()->role;
            switch(Auth::user()->role){
                case 'super_user':
                    return redirect()->route('view-index');
                    // return redirect()->route('view.index1');
                break;
                case 'operator':
                    return redirect()->route('view-index');
                    // return redirect()->route('view.index2');
                break;
                case 'manager':
                    return redirect()->route('view-index');
                    // return redirect()->route('view.index3');
                break;
                default:
                    return redirect()->route('notfound');
                break;
            }
        }else{
            // return 'saddksa';
            return redirect()->route('get-login');
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('get-login');
    }
}
