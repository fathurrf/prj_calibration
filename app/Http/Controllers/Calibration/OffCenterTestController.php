<?php

namespace App\Http\Controllers\Calibration;

use App\Http\Controllers\Controller;
use App\Model\M_Distribusi;
use App\Models\M_Cap;
use App\Models\M_Category;
use App\Models\M_Condition;
use App\Models\M_Detail_Oct;
use App\Models\M_Detail_Rt;
use App\Models\M_Instrument;
use App\Models\M_Off_Center_Test;
use App\Models\M_Position;
use App\Models\M_Repeatability_Test;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use PhpParser\Node\Expr\Cast\Double;

// use Yajra\DataTables\Contracts\DataTable;

class OffCenterTestController extends Controller
{
    public function index(Request $request)
    {
        $id = $request->id; 
        $data = M_Category::all();
        $dataperid = M_Category::where('id',$id)->first();
        return view('OffCenterTest.OffCenterTest', compact('data','id','dataperid'));
    }

    public function show(Request $request)
    {
        // return $request;
        
        // return $clientIP = \Request::ip();
        // return $clientIP = \Request::getClientIp(true);
        // return $clientIP = request()->ip();
        $id_category = $request->id;
        $data = M_Condition::
        with([
            'getInstrument' => function($item) use($id_category){
                $item->where('id_category',$id_category)->with('getCategory');
            }
        ])
        ->whereHas('getInstrument', function($item) use($id_category){
            $item->where('id_category',$id_category);
        })
        ->get();
        return DataTables::of($data)
        ->addColumn('calibration_date', function($data){
            return $calibration_date = $data->created_at->format('d M Y');
        })
        ->addColumn('input_hours', function($data){
            return $input_hours = $data->created_at->format('H:i');
        })
        ->addColumn('status', function($data){
            if($data->status == 'open'){
                return '<a class="badge bg-orange">Open</a>';
            }elseif($data->status == 'prepare'){
                return 
                '<a class="waves-effect badge bg-pink">Prepared</a>';
            }elseif($data->status == 'approve'){
                return 
                '<a class="waves-effect badge bg-green">Approved</a>';
            }elseif($data->status == 'submit'){
                return '<a class="badge bg-blue">Submit</a>';
            }
        })
        ->addColumn('action', function($data){
            $save_method = 'edit';
            $instrument_name = $data->getInstrument->instrument_name;
            if($data->status == 'submit'){
                $data_oct = M_Off_Center_Test::where('id_condition', $data->id)->first();
                $id_oct = $data_oct->id;
                $data_rt = M_Repeatability_Test::where('id_condition', $data->id)->first();
                $id_rt = $data_rt->id;
                if(Auth::user()->role == 'manager'){
                    return
                    '<a onclick="detail_oct_table('."'$data->id'".','."'$instrument_name'".')" id="add_oct" data-toggle="modal" data-target="#modal_view_br" class="waves-effect" title="Add Off Center Test"><i class="material-icons" style="color:blue">visibility</i></a>&nbsp;';
                }else{
                    return 
                    '<a onclick="add_oct('."'$data->id'".','."'$instrument_name'".','."'edit'".','."'$id_oct'".','."'$id_rt'".')" id="add_oct" data-toggle="modal" data-target="#modal_add_oct" class="waves-effect" title="Add Off Center Test"><i class="material-icons" style="color:yellow">edit</i></a>&nbsp;'.
                    '<a onclick="detail_oct_table('."'$data->id'".','."'$instrument_name'".')" id="add_oct" data-toggle="modal" data-target="#modal_view_br" class="waves-effect" title="Add Off Center Test"><i class="material-icons" style="color:blue">visibility</i></a>&nbsp;'.
                    '<a onclick="delete_oct('."'$id_oct'".','."'$id_rt'".','."'$data->id'".')" id="delete_oct" class="waves-effect" title="Delete"><i class="material-icons" style="color:red">delete</i></a>'.
                    '<a onclick="prepare_oct('."'$data->id'".')" id="prepare_oct" class="waves-effect" title="Prepare"><i class="material-icons" style="color:green">check</i></a>';
                }
            }elseif($data->status == 'prepare'){
                $id_oct = null;
                if(Auth::user()->role == 'operator'){
                    return 
                    '<a onclick="detail_oct_table('."'$data->id'".','."'$instrument_name'".')" id="add_oct" data-toggle="modal" data-target="#modal_view_br" class="waves-effect" title="Add Off Center Test"><i class="material-icons" style="color:blue">visibility</i></a>&nbsp;';
                }else{
                    return 
                    '<a onclick="approve_oct('."'$data->id'".')" id="approve_oct" class="waves-effect" title="Approve"><i class="material-icons" style="color:green">check</i></a>'.
                    '<a onclick="reject_oct('."'$data->id'".')" id="reject_oct" class="waves-effect" title="Reject"><i class="material-icons" style="color:red">close</i></a>'.
                    '<a onclick="detail_oct_table('."'$data->id'".','."'$instrument_name'".')" id="add_oct" data-toggle="modal" data-target="#modal_view_br" class="waves-effect" title="Add Off Center Test"><i class="material-icons" style="color:blue">visibility</i></a>&nbsp;';
                }
            }elseif($data->status == 'approve'){
                $id_oct = null;
                return 
                // '<a onclick="detail_oct_table('."'$data->id'".','."'$instrument_name'".')" id="add_oct" data-toggle="modal" data-target="#modal_view_br" class="waves-effect" title="Add Off Center Test"><i class="material-icons" style="color:blue">visibility</i></a>&nbsp;'.
                '<a onclick="sertificateReport('."'$data->id'".')" id="sertificate_print" class="waves-effect" title="Sertificate Print"><i class="material-icons" style="color:black">print</i></a>'.
                '<a onclick="checklistReport('."'$data->id'".')" id="checklist_print" class="waves-effect" title="Checklist Print"><i class="material-icons" style="color:black">print</i></a>'.
                '<a onclick="checklistReport('."'$data->id'".')" id="checklist_print" class="waves-effect" title="History Print"><i class="material-icons" style="color:black">print</i></a>';
            }else{
                $id_oct = null;
                $id_rt = null;
                if(Auth::user()->role == 'manager'){
                    return '<a class="badge bg-teal">In Progress</a>';
                }else{
                    return 
                    '<a onclick="add_oct('."'$data->id'".','."'$instrument_name'".','."'add'".','."'$id_oct'".','."'$id_rt'".')" id="add_oct" data-toggle="modal" data-target="#modal_add_oct" class="waves-effect" title="Add Off Center Test"><i class="material-icons" style="color:orange">add_circle_outline</i></a>&nbsp;';
                }
            }
            
        })
        ->rawColumns(['calibration_date','input_hours','status','action'])
        ->make(true);
    }

    public function getPosition()
    {
        $data = M_Position::all();
        return response()->json($data);
    }

    public function getCap()
    {
        $data = M_Cap::all();
        return response()->json($data);
    }

    public function showDetailOct(Request $request)
    {
        $id_condition = $request->id;
        $data = M_Detail_Oct::
        with([
            'getOct' => function($item){
                $item->with([
                    'getCondition' => function($item2){
                        $item2->with('getInstrument');
                    }
                ]);
            }
        ])
        ->with('getPosition')
        ->whereHas(
            'getOct' , function($item) use($id_condition){
            $item->whereHas(
                'getCondition', function($item2) use($id_condition){
                    $item2->where('id_condition', $id_condition);
                });
            }
        )
        ->get();
        
        return DataTables::of($data)
        ->make(true);
    }

    public function showDetailRt(Request $request)
    {
        $id_condition = $request->id;
        $data = M_Detail_Rt::
        with([
            'getRt' => function($item){
                $item->with([
                    'getCondition' => function($item2){
                        $item2->with('getInstrument');
                    }
                ]);
            }
        ])
        ->with('getCap')
        ->whereHas(
            'getRt' , function($item) use($id_condition){
            $item->whereHas(
                'getCondition', function($item2) use($id_condition){
                    $item2->where('id_condition', $id_condition);
                });
            }
        )
        ->get();
        
        return DataTables::of($data)
        ->make(true);
    }

    public function postOct(Request $request)
    {
        // return $request;
        $id_user = Auth::user()->id;
        if($request->input('id_oct') && $request->input('id_rt')){
            // return 'update';
            $update_oct = M_Off_Center_Test::
            where('id',$request->id_oct)
            ->update([
                'weight_standard' => $request->weight_standard,
                'id_user_edit' => $id_user
            ]);

            $update_rt = M_Repeatability_Test::
            where('id',$request->id_rt)
            ->update([
                'id_user_edit' => $id_user
            ]);

            $countoct = 0;
            for($i=0; $i<= count($request['id_position'])-1; $i++){
                $update = M_Detail_Oct::
                where('id_oct',$request->id_oct)
                ->where('id_position', $request['id_position'][$i])
                ->update([
                    'br1' => $request['br1'][$i],
                    'br2' => $request['br2'][$i],
                    'br3' => $request['br3'][$i],
                    'id_user_edit' => $id_user
                ]);
                $countoct++;
            }

            $countrt = 0;
            for($i=0; $i<= count($request['id_cap'])-1; $i++){
                $update = M_Detail_Rt::
                where('id_rt',$request->id_rt)
                ->where('id_cap', $request['id_cap'][$i])
                ->update([
                    'value' => $request['valuert'][$i],
                    'certificate_standard' => $request['u95_ss'][$i],
                    'br1' => $request['br_1'][$i],
                    'br2' => $request['br_2'][$i],
                    'br3' => $request['br_3'][$i],
                    'id_user_edit' => $id_user
                ]);
                $countrt++;
            }

            if($countoct == count($request['id_position']) && $countrt == count($request['id_cap'])){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil diupdate',
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal disimpan'
                ]);
            }
        }else{
            // return 'add';
            $simpan_oct = new M_Off_Center_Test();
            $simpan_oct->id_condition = $request->id;
            $simpan_oct->weight_standard = $request->weight_standard;
            $simpan_oct->id_user = $id_user;
            $simpan_oct->save();
            $id_octs = $simpan_oct->id;
            $id_condition = $simpan_oct->id_condition;

            $simpan_rt = new M_Repeatability_Test();
            $simpan_rt->id_condition = $request->id;
            $simpan_rt->id_user = $id_user;
            $simpan_rt->save();
            $id_rts = $simpan_rt->id;
            $id_condition = $simpan_rt->id_condition;

            $updatestatus = M_Condition::
            where('id',$id_condition)
            ->update([
                'status' => 'submit'
            ]);

            $countoct = 0;
            for($i=0; $i<= count($request['id_position'])-1; $i++) {
                $simpan = new M_Detail_Oct();
                $simpan->id_oct = $id_octs; 
                $simpan->id_position = $request['id_position'][$i]; 
                $simpan->br1 = $request['br1'][$i]; 
                $simpan->br2 = $request['br2'][$i]; 
                $simpan->br3 = $request['br3'][$i];
                $simpan->id_user = $id_user;
                $simpan->save();
                $countoct++;
            }

            $countrt = 0;
            for($i=0; $i<= count($request['id_cap'])-1; $i++) {
                $simpan = new M_Detail_Rt();
                $simpan->id_rt = $id_rts; 
                $simpan->id_cap = $request['id_cap'][$i]; 
                $simpan->value = $request['valuert'][$i]; 
                $simpan->certificate_standard = $request['u95_ss'][$i]; 
                $simpan->br1 = $request['br_1'][$i]; 
                $simpan->br2 = $request['br_2'][$i]; 
                $simpan->br3 = $request['br_3'][$i];
                $simpan->id_user = $id_user;
                $simpan->save();
                $countrt++;
            }

            if($countoct == count($request['id_position']) && $countrt == count($request['id_cap'])){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil disimpan',
                    // 'id_condition'   => $id_condition
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal disimpan'
                ]);
            }
        }
    }

    public function getDetailOct(Request $request)
    {
        $id_oct = $request->id;
        $data = M_Off_Center_Test::
        where('id',$id_oct)
        ->with([
            'getDetailOct' => function($item){
                $item->with('getPosition');
            }
        ])
        ->first();
        return response()->json($data);
    }

    public function getDetailRt(Request $request)
    {
        $id_rt = $request->id_rt;
        $data = M_Repeatability_Test::
        where('id',$id_rt)
        ->with([
            'getDetailRt' => function($item){
                $item->with('getCap');
            }
        ])
        ->first();
        return response()->json($data);
    }

    public function deleteOct(Request $request)
    {
        // return $request;
        $delete_detail_oct = M_Detail_Oct::where('id_oct',$request->id_oct)->delete();
        $delete_detail_oct = M_Detail_Rt::where('id_rt',$request->id_rt)->delete();
        $delete_oct = M_Off_Center_Test::where('id',$request->id_oct)->delete();
        $delete_rt = M_Repeatability_Test::where('id',$request->id_rt)->delete();
        $updatestatus = M_Condition::
            where('id',$request->id_cond)
            ->update([
                'status' => 'open'
            ]);
        return response()->json([
            'code'      => 200,
            'status'    => 'success',
            'message'   => 'Data berhasil dihapus',
        ]);
    }

    public function prepareOct(Request $request)
    {
        // return $request;
        $id_user = Auth::user()->id;
        $updatestatus = M_Condition::
            where('id',$request->id)
            ->update([
                'status' => 'prepare',
                'id_user_prepare' => $id_user 
            ]);
        return response()->json([
            'code'      => 200,
            'status'    => 'success',
            'message'   => 'Data berhasil diapprove',
        ]);
    }

    public function approveOct(Request $request)
    {
        // return $request;
        $id_user = Auth::user()->id;
        $updatestatus = M_Condition::
            where('id',$request->id)
            ->update([
                'status' => 'approve',
                'id_user_approve' => $id_user 
            ]);
        return response()->json([
            'code'      => 200,
            'status'    => 'success',
            'message'   => 'Data berhasil diapprove',
        ]);
    }

    public function certificateReport(Request $request){
        
        $pdf = PDF::loadView('Report.certificate_report');
		return $pdf->setPaper([0, 0, 468, 612], 'potrait')->setWarnings(false)->stream();
    }

    public function checklistReport(Request $request){
        // return $request;
        $data = M_Condition::
        where('id',$request->id)
        ->with('getInstrument')
        ->with('getUserPrepare')
        ->with('getUserApprove')
        ->with([
            'getRt' => function ($item){
                $item->with([
                    'getDetailRt' => function ($item){
                        $item->with('getCap')->get();
                    }
                ]);
            }
        ])
        ->with([
            'getOct' => function ($item){
                $item->with([
                    'getDetailOct' => function ($item){
                        $item->with('getPosition')->get();
                    }
                ]);
            }
        ])
        ->first();

        $oct = collect($data->getOct->getDetailOct)->map(function($val){
            return
            [
                'getPosition' => $val->getPosition,
                'br1'   => (int) $val->br1,
                'br2'   => (int) $val->br2,
                'br3'   => (int) $val->br3,
                'avg'   => (double) number_format((($val->br1 + $val->br2 + $val->br3) / 3),2)
            ];
        });
        $max = [
            'br1' => $oct->max(['br1']),
            'br2' => $oct->max(['br2']),
            'br3' => $oct->max(['br3']),
            'avg' => $oct->max(['avg']),
        ];

        $rt = collect($data->getRt->getDetailRt)->map(function($val){
            return
            [
                'cap'   => $val->getCap->cap,
                'value' => $val->value,
                'u95_cs'=> $val->certificate_standard,
                'br1'   => $br_1 = (float) $val->br1,
                'br2'   => $br_2 = (float) $val->br2,
                'br3'   => $br_3 = (float) $val->br3,
                'sum'   => $sum = (float) $br_1 + $br_2 + $br_3,
                'br1_2' => $br1_2 = (float) $br_1 * $br_1,
                'br2_2' => $br2_2 = (float) $br_2 * $br_2,
                'br3_2' => $br3_2 = (float) $br_3 * $br_3,
                'sumbr2'=> $sumbr2 = (float) $br1_2 + $br2_2 + $br3_2,
                'sum2'  => $sum2 = (float) $sum * $sum,
                's2'    => $s2 = (float) ((3 * $sumbr2) - $sum2) / 6,
                'stdev' => (float) number_format(sqrt($s2),4)
            ];
        });

        $data_U95 = [
            'stdevmax'      => $stdevmax = $rt->max(['stdev']),
            'ustandard'     => $ustandard = $rt->max(['u95_cs']),
            'n'             => $n = 3,
            'kstandard'     => $kstandard = 1.98,
            'ua'            => $ua = (float) number_format($stdevmax/(sqrt(3)),9),
            'ucertificate'  => $ucertificate = (float) number_format(($ustandard / $kstandard),5),
            'va'            => $va = (float) $rt->count(['value']),
            'vcertificate'  => $vcertificate = 0.5 * 100,
            'res'           => $res = (float) $data->getInstrument->resolution,
            'distrect'      => $distrect = (float) number_format(sqrt($va),3),
            'ures'          => $ures = (float) number_format((0.5 * ($res / $distrect)),5),
            'vres'          => $vres = (float) 50,
            'uc'            => $uc = (float) number_format(sqrt(pow($ua,2) + pow($ucertificate,2) + pow($ures,2)),5),
            'veff'          => $veff = (float) number_format((pow($uc,4)) / ((pow($ua,4)) / $va + (pow($ucertificate,4)/$vcertificate) + (pow($ures,4)/$vres)),2),
            'push_veff'     => $push_veff = collect($distribusi = M_Distribusi::all())->map(function($val){
                                    return 
                                    [
                                        'df'            => (double) $val->df,
                                        'probabilitas'  => (double) $val->probabilitas
                                    ];
                                })->where('df', floor($veff))->first(),
            'keff'          => $keff = $push_veff['probabilitas'],
            'u95'           => $keff * $veff
        ];

        $pdf = PDF::loadView('Report.checklist_report', compact('data','rt','oct','max','data_U95'));
        // $pdf = PDF::loadView('Report.checklist_report',(['data'=>$data]));
		return $pdf->setPaper([0, 0, 468, 612], 'potrait')->setWarnings(false)->stream();
    }
}
