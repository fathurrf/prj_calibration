<?php

namespace App\Http\Controllers\Calibration;

use App\Http\Controllers\Controller;
use App\Models\M_Category;
use App\Models\M_Department;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class DepartmentController extends Controller
{
    public function index()
    {
        $data = M_Category::all();
        return view('Department.Department',compact('data'));
    }

    public function show()
    {
        $data = M_Department::all();
        return DataTables::of($data)
        ->addColumn('action', function($data){
            $save_method = 'edit';
            return 
            '<a onclick="add_department('."'$data->id'".','."'$save_method'".')" id="edit_department" data-toggle="modal" data-target="#modal_add_department" class="waves-effect" title="Edit Department"><i class="material-icons" style="color:yellow">edit</i></a>&nbsp;'.
            '<a onclick="delete_department('."'$data->id'".')" id="delete_department" class="waves-effect" title="Delete Department"><i class="material-icons" style="color:red">delete</i></a>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function post(Request $request){
        // return $request;
        $id_user = Auth::user()->id;
        if($request->input('id')){
            $update = M_Department::where('id', $request->id)
            ->update([
                'department_name' => $request->department_name,
                'id_user_edit'=> $id_user,
            ]);
            // $id_user = $update->id;
            if($update){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil diupdate',
                    // 'id_user'   => $id_user
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal diupdate'
                ]);
            }
        }else{
            $simpan = new M_Department();
            $simpan->department_name  = $request->department_name;
            $simpan->id_user = $id_user;
            $simpan->save();
            $id_department = $simpan->id;
            if($id_department){
                return response()->json([
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'Data berhasil disimpan',
                    'id_department'   => $id_department
                ]);
            }else{
                return response()->json([
                    'code'      => 200,
                    'status'    => 'failed',
                    'message'   => 'Data gagal disimpan'
                ]);
            }
        }
    }

    public function showEdit(Request $request)
    {
        // return $request;
        $data = M_Department::where('id',$request->id)->first();
        return response()->json($data);
    }
    
    public function delete(Request $request)
    {
        // return $request;
        $data = M_Department::where('id',$request->id)->delete();

        return response()->json([
            'code'      => 200,
            'status'    => 'success',
            'message'   => 'Data berhasil dihapus',
            'data'      => $data
        ]);
    }
}
