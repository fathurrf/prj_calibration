@extends('index')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                {{-- Off Center Test <strong><a href="#">{{strtoupper($dataperid->category_name)}}</a></strong> --}}
                <strong><a href="#">{{strtoupper($dataperid->category_name)}}</a> {{strtoupper('Off Center Test')}}</strong> 
                {{-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> --}}
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    {{-- <div class="header"> --}}
                        {{-- <h2>
                            Users Data
                        </h2> --}}
                        {{-- <button onclick="add_instrument()" id="btn_add_instrument" type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#modal_add_instrument">
                            <i class="material-icons">add_circle</i>
                            <span>Add Instrument</span>
                        </button> --}}
                        {{-- <button type="btn_add_user" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#modal_add_user">
                            MODAL - DEFAULT SIZE</button> --}}
                    {{-- </div> --}}
                    <div class="body">
                        <div class="table-responsive">
                            <table id="oct_table" cellspacing="0" width="100%" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Instrument Name</th>
                                        <th>Calibration Date</th>
                                        <th>Input Hours</th>
                                        <th>Location</th>
                                        <th>Temp</th>
                                        <th>Humidity</th>
                                        <th>Status</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

        <div class="modal fade" id="modal_add_oct" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel"><b>Form Detail Condition </b><b><a class="instrument_name"></a></b></h4>
                    </div>
                    <hr>
                    <div class="modal-body">
                        {{-- S: tabs --}}
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row clearfix">
                                    <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                        <form id="form_oct" name="form_oct">
                                        @csrf
                                            <div class="panel-group" id="accordionForm" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-col-teal">
                                                    <div class="panel-heading" role="tab" id="headingFormOct">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordionForm" href="#collapseFormOct" aria-expanded="true" aria-controls="collapseFormOct">
                                                                Form Input Off Center Test
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseFormOct" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFormOct">
                                                        <div class="panel-body">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <div class="form-control">
                                                                        <label class="card-inside-title">Weight Standard</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <div class="form-group">
                                                                        <div class="form-control">
                                                                        <label class="card-inside-title">:</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <input data-rule="quantity" type="text" id="weight_standard" name="weight_standard" class="form-control" placeholder="Weight Standard" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <div class="form-control">
                                                                            <label class="card-inside-title">Position</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-9 text-center">
                                                                    <div class="form-group">
                                                                        <div class="form-control">
                                                                            <label class="card-inside-title">Balancing Reading</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix" id="position_group"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-col-teal">
                                                    <div class="panel-heading" role="tab" id="headingFormRt">
                                                        <h4 class="panel-title">
                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionForm" href="#collapseFormRt" aria-expanded="false" aria-controls="collapseFormRt">
                                                                Form Input Repeatability Test
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseFormRt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFormRt">
                                                        <div class="panel-body">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <div class="form-control">
                                                                            <label class="card-inside-title">Cap</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2 text-center">
                                                                    <div class="form-group">
                                                                        <div class="form-control">
                                                                            <label class="card-inside-title">Value</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2 text-center">
                                                                    <div class="form-group">
                                                                        <div class="form-control">
                                                                            <label class="card-inside-title">U95</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6 text-center">
                                                                    <div class="form-group">
                                                                        <div class="form-control">
                                                                            <label class="card-inside-title">Balancing Reading</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix" id="cap_group"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button onClick="simpan_oct()" type="button" id="btn_simpan_oct" class="btn btn-link waves-effect">SAVE</button>
                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- E: tabs --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal_view_br" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel"><b>View Detail Condition </b><b><a class="instrument_name"></a></b></h4>
                    </div>
                    <hr>
                    <div class="modal-body">
                        {{-- S: tabs --}}
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                    <li role="presentation" class="active"><a href="#viewTable" data-toggle="tab">View Table</a></li>
                                    <li role="presentation"><a href="#viewDetail" data-toggle="tab">View Detail</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane animated flipInX active" id="viewTable">
                                        <div class="row clearfix">
                                            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                                <form id="form_oct" name="form_oct">
                                                @csrf
                                                    <div class="panel-group" id="accordionTable" role="tablist" aria-multiselectable="true">
                                                        <div class="panel panel-col-teal">
                                                            <div class="panel-heading" role="tab" id="headingTableOct">
                                                                <h4 class="panel-title">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordionTable" href="#collapseTableOct" aria-expanded="true" aria-controls="collapseTableOct">
                                                                        View Table Off Center Test
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseTableOct" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTableOct">
                                                                <div class="panel-body">
                                                                    <div class="table-responsive">
                                                                        <table id="detailoct_table" cellspacing="0" width="100%" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No</th>
                                                                                    <th>Position</th>
                                                                                    <th>BR1</th>
                                                                                    <th>BR2</th>
                                                                                    <th>BR3</th>
                                                                                </tr>
                                                                            </thead>
                                                                        </table>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-col-teal">
                                                            <div class="panel-heading" role="tab" id="headingTableRt">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionTable" href="#collapseTableRt" aria-expanded="false" aria-controls="collapseTableRt">
                                                                        View Table Repeatability Test
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseTableRt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTableRt">
                                                                <div class="panel-body">
                                                                    <div class="table-responsive">
                                                                        <table id="detailrt_table" cellspacing="0" width="100%" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No</th>
                                                                                    <th>Position</th>
                                                                                    <th>Value</th>
                                                                                    <th>U95 SS</th>
                                                                                    <th>BR1</th>
                                                                                    <th>BR2</th>
                                                                                    <th>BR3</th>
                                                                                </tr>
                                                                            </thead>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- <div class="modal-footer">
                                                        <button onClick="simpan_oct()" type="button" id="btn_simpan_oct" class="btn btn-link waves-effect">SAVE</button>
                                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                                                    </div> --}}
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane animated flipInX" id="viewDetail">
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="card">
                                                    <div class="header bg-teal">
                                                        <h2>Repeatability</h2>
                                                    </div>
                                                    <div class="body">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">StdDev max<span class="badge bg-teal">0.004583</span></li>
                                                            <li class="list-group-item">n<span class="badge bg-teal">3</span></li>
                                                            <li class="list-group-item">Ua<span class="badge bg-teal">0.002645751</span></li>
                                                            <li class="list-group-item">Va<span class="badge bg-teal">8</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="card">
                                                    <div class="header bg-teal">
                                                        <h2>Standard Certificate</h2>
                                                    </div>
                                                    <div class="body">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">Ustandard<span class="badge bg-teal">0.001</span></li>
                                                            <li class="list-group-item">k -standard<span class="badge bg-teal">1.98</span></li>
                                                            <li class="list-group-item">Ucertificate<span class="badge bg-teal">0.00051</span></li>
                                                            <li class="list-group-item">Vcertificate<span class="badge bg-teal">50</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="card">
                                                    <div class="header bg-teal">
                                                        <h2>Readability</h2>
                                                    </div>
                                                    <div class="body">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">Res<span class="badge bg-teal">0.001</span></li>
                                                            <li class="list-group-item">Dist. Rect<span class="badge bg-teal">2.828</span></li>
                                                            <li class="list-group-item">Ures<span class="badge bg-teal">0.00018</span></li>
                                                            <li class="list-group-item">Vres<span class="badge bg-teal">50</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="card">
                                                    <div class="header bg-teal">
                                                        <h2>Uncertainly</h2>
                                                    </div>
                                                    <div class="body">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">Uc<span class="badge bg-teal">0.00270</span></li>
                                                            <li class="list-group-item">Veff<span class="badge bg-teal">8.67</span></li>
                                                            <li class="list-group-item">k-eff<span class="badge bg-teal">2,3060041</span></li>
                                                            <li class="list-group-item">U95<span class="badge bg-teal">0.0062</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        {{-- E: tabs --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- JQuery Steps Plugin Js -->
<script src="{{asset('template/assets/plugins/jquery-steps/jquery.steps.js')}}"></script>

<!-- Custom Js -->
{{-- <script src="{{asset('template/assets/js/admin.js')}}"></script> --}}
{{-- <script src="{{asset('template/assets/js/pages/forms/form-wizard.js')}}"></script> --}}

<!-- Demo Js -->
<script src="{{asset('template/assets/js/demo.js')}}"></script>

@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function() {
        oct_table();
        // condition_table();
    })

    var id_category = {!! $id !!}

    function oct_table(){
        console.log(id_category)
        url = '{{url("OffCenterTest/show")}}?id='+id_category
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return{
                "iStart"        : oSettings._iDisplayStart,
                "iEnd"          : oSettings.fnDisplayEnd(),
                "iLenght"       : oSettings._iDisplayLength,
                "iTotal"        : oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage"         : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages"   : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            }
        }

        var table = $('#oct_table').DataTable({
            processing  : true,
            serverSide  : false,
            bDestroy    : true,
            ajax        : url,
            columns     : [
                {
                    "data"      : null,
                    "width"     : "10px",
                    "sClass"    : "text-center",
                    "orderable" : false
                },
                { 
                    data    : 'get_instrument.instrument_name',
                    name    : 'instrument_name'
                },
                { 
                    data    : 'calibration_date',
                    name    : 'calibration_date'
                },
                { 
                    data    : 'input_hours',
                    name    : 'input_hours'
                },
                { 
                    data    : 'location',
                    name    : 'location'
                },
                { 
                    data    : 'temp',
                    name    : 'temp'
                },
                { 
                    data    : 'humidity',
                    name    : 'humidity'
                },
                { 
                    data    : 'status',
                    name    : 'status'
                },
                { 
                    data    : 'action',
                    name    : 'action'
                },
            ],
            "order":[
                [1,'asc']
            ],
            "rowCallback": function(row, data, iDisplayIndex){
                var info    = this.fnPagingInfo();
                var page    = info.iPage;
                var lenght  = info.iLenght;
                var index   = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    }
    
    // S: Condition
    var save_method;
    var id_condition
    var instrument_name
    var id_off_center_test
    var id_repeatability_test
    function add_oct(id_cond,inst_name, sv_mtd, id_oct, id_rt){
        id_condition = id_cond;
        instrument_name = inst_name;
        save_method = sv_mtd;
        id_off_center_test = id_oct;
        id_repeatability_test = id_rt;
        $('.instrument_name').text(instrument_name)
        if(save_method == 'add'){
            console.log(id_oct)
            $('#weight_standard').val('')
            uurl = '{{route("get-position")}}'
            $.ajax({
                url:uurl,
                type:"GET",
                dataType: "JSON",
                async:true,
                success:function(data){
                    var i;
                    var html = "";
                    for (i=0;i<data.length;++i){
                        html +=
                        '<div class="col-sm-2 position_name_group">'+
                        '<div class="form-group">'+
                        '<div class="form-control">'+
                        '<label id="position_name" class="card-inside-title">'+data[i].position+'</label>'+
                        '<input type="hidden" id="id_position" name="id_position[]" value="'+data[i].id+'" class="form-control br1" placeholder="1nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-1 batas_group">'+
                        '<div class="form-group">'+
                        '<div class="form-control-label">'+
                        '<label class="card-inside-title">:</label>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-3 br1_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br1" name="br1[]" class="form-control br1" placeholder="1nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-3 br2_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br2" name="br2[]" class="form-control br1" placeholder="2nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-3 br3_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br3" name="br3[]" class="form-control br1" placeholder="3nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                    }
                    $('#position_group').html(html)
                }
            })

            uurl = '{{route("get-cap")}}'
            $.ajax({
                url:uurl,
                type:"GET",
                dataType: "JSON",
                async:true,
                success:function(data){
                    var i;
                    var html = "";
                    for (i=0;i<data.length;++i){
                        html +=
                        '<div class="col-sm-1 cap_name_group">'+
                        '<div class="form-group">'+
                        '<div class="form-control">'+
                        '<label id="cap_name" class="card-inside-title">'+data[i].cap+'</label>'+
                        '<input type="hidden" id="id_cap" name="id_cap[]" value="'+data[i].id+'" class="form-control br1" placeholder="1nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-1 batas_group">'+
                        '<div class="form-group">'+
                        '<div class="form-control-label">'+
                        '<label class="card-inside-title">:</label>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 valuert_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="valuert" name="valuert[]" class="form-control valuert" placeholder="value" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 u95_sertificate_standard_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="u95_ss" name="u95_ss[]" class="form-control u95_ss" placeholder="U95 Sertificate Standard" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 br_1_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br_1" name="br_1[]" class="form-control br_1" placeholder="1nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 br_2_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br_2" name="br_2[]" class="form-control br_2" placeholder="2nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 br_3_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br_3" name="br_3[]" class="form-control br_3" placeholder="3nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                    }
                    $('#cap_group').html(html)
                }
            })
        }else{
            console.log(id_oct)
            uurl = '{{url("OffCenterTest/getdetailoct")}}?id='+id_oct;
            $.ajax({
                url : uurl,
                type : "GET",
                dataType: "JSON",
                async:true,
                success:function(data){
                    data_br = data.get_detail_oct
                    console.log(data_br)
                    var i;
                    var html = "";
                    $('#weight_standard').val(data.weight_standard)
                    for (i=0;i<data_br.length;++i){
                        html +=
                        '<div class="col-sm-2 position_name_group">'+
                        '<div class="form-group">'+
                        '<div class="form-control">'+
                        '<label id="position_name" class="card-inside-title">'+data_br[i].get_position.position+'</label>'+
                        '<input type="hidden" id="id_position" name="id_position[]" value="'+data_br[i].id_position+'" class="form-control br1" placeholder="1nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-1 batas_group">'+
                        '<div class="form-group">'+
                        '<div class="form-control-label">'+
                        '<label class="card-inside-title">:</label>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-3 br1_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br1" name="br1[]" value="'+data_br[i].br1+'" class="form-control br1" placeholder="1nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-3 br2_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br2" name="br2[]" value="'+data_br[i].br2+'" class="form-control br1" placeholder="2nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-3 br3_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br3" name="br3[]" value="'+data_br[i].br3+'" class="form-control br1" placeholder="3nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                    }
                    $('#position_group').html(html)
                }
            })

            console.log(id_rt)
            uurl = '{{url("OffCenterTest/getdetailrt")}}?id_rt='+id_rt;
            $.ajax({
                url : uurl,
                type : "GET",
                dataType: "JSON",
                async:true,
                success:function(data){
                    data_br = data.get_detail_rt
                    var i;
                    var html = "";
                    for (i=0;i<data_br.length;++i){
                        html +=
                        '<div class="col-sm-1 cap_name_group">'+
                        '<div class="form-group">'+
                        '<div class="form-control">'+
                        '<label id="cap_name" class="card-inside-title">'+data_br[i].get_cap.cap+'</label>'+
                        '<input type="hidden" id="id_cap" name="id_cap[]" value="'+data_br[i].id_cap+'" class="form-control br1" placeholder="1nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-1 batas_group">'+
                        '<div class="form-group">'+
                        '<div class="form-control-label">'+
                        '<label class="card-inside-title">:</label>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 valuert_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="valuert" name="valuert[]" value="'+data_br[i].value+'" class="form-control valuert" placeholder="value" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 u95_sertificate_standard_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="u95_ss" name="u95_ss[]" value="'+data_br[i].certificate_standard+'" class="form-control u95_ss" placeholder="U95 Sertificate Standard" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 br_1_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br_1" name="br_1[]" value="'+data_br[i].br1+'" class="form-control br_1" placeholder="1nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 br_2_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br_2" name="br_2[]" value="'+data_br[i].br2+'" class="form-control br_2" placeholder="2nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-2 br_3_group">'+
                        '<div class="form-group">'+
                        '<div class="form-line">'+
                        '<input type="text" id="br_3" name="br_3[]" value="'+data_br[i].br3+'" class="form-control br_3" placeholder="3nd" />'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                    }
                    $('#cap_group').html(html)
                }
            })
        }
    }

    function simpan_oct(){
        console.log('asdjk')
        var form_data = $('#form_oct').serialize();
        if (save_method == 'add'){
            var url = "{{url('OffCenterTest/postoct?id=')}}"+id_condition;
        }
        else{
            var url = "{{url('OffCenterTest/postoct?id_oct=')}}"+id_off_center_test+"&id_rt="+id_repeatability_test ;
        }
        console.log(form_data)
        $.ajax({
            url     : url,
            type    : 'POST',
            data    : form_data,
            async   : false,
            success : function(data){
                console.log(data)
                if(data.status = "success"){
                    $("#form_oct")[0].reset();
                    $('#oct_table').DataTable().ajax.reload();
                    $.notify(data.message, "success");
                    add_oct(id_condition,instrument_name,'add')
                }else{
                    $('#oct_table').DataTable().ajax.reload();
                    $.notify(data.message, "danger");
                }
            },
            error: function(data) {
                $.notify(data.message, "danger");
            }
        });
    }

    function detail_oct_table(id,inst_name){
        instrument_name = inst_name;
        $('.instrument_name').text(instrument_name)
        url = "{{url('OffCenterTest/showdetailoct')}}?id="+id;
        url_rt = "{{url('OffCenterTest/showdetailrt')}}?id="+id;
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return{
                "iStart"        : oSettings._iDisplayStart,
                "iEnd"          : oSettings.fnDisplayEnd(),
                "iLenght"       : oSettings._iDisplayLength,
                "iTotal"        : oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage"         : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages"   : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            }
        }

        var table = $('#detailoct_table').DataTable({
            processing  : true,
            serverSide  : false,
            bDestroy    : true,
            ajax        : url,
            columns     : [
                {
                    "data"      : null,
                    "width"     : "10px",
                    "sClass"    : "text-center",
                    "orderable" : false
                },
                { 
                    data    : 'get_position.position',
                    name    : 'position'
                },
                { 
                    data    : 'br1',
                    name    : 'br1'
                },
                { 
                    data    : 'br2',
                    name    : 'br2'
                },
                { 
                    data    : 'br3',
                    name    : 'br3'
                },
            ],
            "order":[
                [1,'asc']
            ],
            "rowCallback": function(row, data, iDisplayIndex){
                var info    = this.fnPagingInfo();
                var page    = info.iPage;
                var lenght  = info.iLenght;
                var index   = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        var tablert = $('#detailrt_table').DataTable({
            processing  : true,
            serverSide  : false,
            bDestroy    : true,
            ajax        : url_rt,
            columns     : [
                {
                    "data"      : null,
                    "width"     : "10px",
                    "sClass"    : "text-center",
                    "orderable" : false
                },
                { 
                    data    : 'get_cap.cap',
                    name    : 'cap'
                },
                { 
                    data    : 'value',
                    name    : 'value'
                },
                { 
                    data    : 'certificate_standard',
                    name    : 'certificate_standard'
                },
                { 
                    data    : 'br1',
                    name    : 'br1'
                },
                { 
                    data    : 'br2',
                    name    : 'br2'
                },
                { 
                    data    : 'br3',
                    name    : 'br3'
                },
            ],
            "order":[
                [1,'asc']
            ],
            "rowCallback": function(row, data, iDisplayIndex){
                var info    = this.fnPagingInfo();
                var page    = info.iPage;
                var lenght  = info.iLenght;
                var index   = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    }

    function delete_oct(id_oct, id_rt, id_cd){
        swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },function(){
            $.ajax({
                type: "GET",
                url: "{{url('OffCenterTest/deleteoct?id_oct=')}}"+id_oct+'&id_rt='+id_rt+'&id_cond='+id_cd,
                dataType: "JSON",
                success: function (data) {
                    console.log(data)
                    swal("Deleted!", data.message, "success");
                    $('#oct_table').DataTable().ajax.reload();
                }         
            });
        });
    }

    function prepare_oct(id){
        swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Prepare it!",
            closeOnConfirm: false
        },function(){
            $.ajax({
                type: "GET",
                url: "{{url('OffCenterTest/prepareoct?id=')}}"+id,
                dataType: "JSON",
                success: function (data) {
                    console.log(data)
                    swal("Prepared!", data.message, "success");
                    $('#oct_table').DataTable().ajax.reload();
                }         
            });
        });
    }

    function approve_oct(id){
        swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Approve it!",
            closeOnConfirm: false
        },function(){
            $.ajax({
                type: "GET",
                url: "{{url('OffCenterTest/approveoct?id=')}}"+id,
                dataType: "JSON",
                success: function (data) {
                    console.log(data)
                    swal("Approved!", data.message, "success");
                    $('#oct_table').DataTable().ajax.reload();
                }         
            });
        });
    }

    function sertificateReport(id){
        url= "{{url('OffCenterTest/certificatereport')}}?id="+id;
        window.open(url);
    }

    function checklistReport(id){
        url= "{{url('OffCenterTest/checklistreport')}}?id="+id;
        window.open(url);
    }
    // E: Condition
</script>
@endpush