@extends('index')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Off Center Test
                {{-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> --}}
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        {{-- <h2>
                            Users Data
                        </h2> --}}
                        {{-- <button onclick="add_instrument()" id="btn_add_instrument" type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#modal_add_instrument">
                            <i class="material-icons">add_circle</i>
                            <span>Add Instrument</span>
                        </button> --}}
                        {{-- <button type="btn_add_user" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#modal_add_user">
                            MODAL - DEFAULT SIZE</button> --}}
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="oct_table" cellspacing="0" width="100%" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Instrument ID</th>
                                        <th>Manufacturer</th>
                                        <th>Model No</th>
                                        <th>Serial No</th>
                                        <th>Max Capacity</th>
                                        <th>Resolution</th>
                                        <th>Type</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

        <div class="modal fade" id="modal_add_condition" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Form Condition <b><a class="instrument_name"></a></b></h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_condition" name="form_condition">
                            @csrf
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="location" name="location" required>
                                    <label class="form-label">Location</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="temp" name="temp" required>
                                    <label class="form-label">Temp</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="humidity" name="humidity" required>
                                    <label class="form-label">Humidity</label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button onClick="simpan_condition()" type="button" id="btn_simpan_condition" class="btn btn-link waves-effect">SAVE</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                            </div>
                        </form>
                        <hr>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="condition_table" cellspacing="0" width="100%" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Calibration Date</th>
                                            <th>Input Hours</th>
                                            <th>Location</th>
                                            <th>Temp</th>
                                            <th>Humidity</th>
                                            <th width="100px">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function() {
        oct_table();
        // condition_table();
    })

    function oct_table(){
        url = '{{route("show-oct")}}'
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return{
                "iStart"        : oSettings._iDisplayStart,
                "iEnd"          : oSettings.fnDisplayEnd(),
                "iLenght"       : oSettings._iDisplayLength,
                "iTotal"        : oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage"         : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages"   : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            }
        }

        var table = $('#oct_table').DataTable({
            processing  : true,
            serverSide  : false,
            bDestroy    : true,
            ajax        : url,
            columns     : [
                {
                    "data"      : null,
                    "width"     : "10px",
                    "sClass"    : "text-center",
                    "orderable" : false
                },
                { 
                    data    : 'instrument_name',
                    name    : 'instrument_name'
                },
                { 
                    data    : 'manufacturer',
                    name    : 'manufacturer'
                },
                { 
                    data    : 'model_no',
                    name    : 'model_no'
                },
                { 
                    data    : 'serial_no',
                    name    : 'serial_no'
                },
                { 
                    data    : 'max_capacity',
                    name    : 'max_capacity'
                },
                { 
                    data    : 'resolution',
                    name    : 'resolution'
                },
                { 
                    data    : 'type',
                    name    : 'type'
                },
                { 
                    data    : 'action',
                    name    : 'action'
                },
            ],
            "order":[
                [1,'asc']
            ],
            "rowCallback": function(row, data, iDisplayIndex){
                var info    = this.fnPagingInfo();
                var page    = info.iPage;
                var lenght  = info.iLenght;
                var index   = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    }
    
    // S: Condition
    var save_method;
    var id_instrument;
    var id_condition
    var instrument_name
    function add_condition(id_inst,inst_name, sv_mtd, id_cond){
        id_instrument = id_inst
        instrument_name = inst_name
        save_method = sv_mtd;
        id_condition = id_cond
        if(save_method == 'add'){
            $('.instrument_name').text(instrument_name)
            $('#location').val('')
            $('#temp').val('')
            $('#humidity').val('')
        }else{
            uurl = '{{url("OffCenterTest/detailcondition")}}?id='+id_condition;
            $.ajax({
                url : uurl,
                type : "GET",
                dataType: "JSON",
                async:true,
                success:function(data){
                    $('#location').val(data.location)
                    $('#temp').val(data.temp)
                    $('#humidity').val(data.humidity)
                }
            })
        }
        condition_table(id_instrument);
    }

    function simpan_condition(){
        // add_condition(id_instrument,instrument_name,save_method,id_condition)
        console.log(id_instrument)
        console.log(instrument_name)
        console.log(save_method)
        console.log(id_condition)
        var form_data = $('#form_condition').serialize();
        if (save_method == 'add'){
            var url = "{{url('OffCenterTest/postcondition?id=')}}"+id_instrument;
        }else{
            var url = "{{url('OffCenterTest/postcondition?id_cond=')}}"+id_condition;
        }
        console.log(form_data)
        $.ajax({
            url     : url,
            type    : 'POST',
            data    : form_data,
            async   : false,
            success : function(data){
                console.log(data)
                if(data.status = "success"){
                    $("#form_condition")[0].reset();
                    $('#condition_table').DataTable().ajax.reload();
                    $.notify(data.message, "success");
                    add_condition(id_instrument,instrument_name,'add',id_condition)
                }else{
                    $('#condition_table').DataTable().ajax.reload();
                    $.notify(data.message, "danger");
                }
            },
            error: function(data) {
                $.notify(data.message, "danger");
            }
        });
    }

    function condition_table(id_instrument){
        url = "{{url('OffCenterTest/showcondition')}}?id="+id_instrument;
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return{
                "iStart"        : oSettings._iDisplayStart,
                "iEnd"          : oSettings.fnDisplayEnd(),
                "iLenght"       : oSettings._iDisplayLength,
                "iTotal"        : oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage"         : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages"   : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            }
        }

        var table = $('#condition_table').DataTable({
            processing  : true,
            serverSide  : false,
            bDestroy    : true,
            ajax        : url,
            columns     : [
                {
                    "data"      : null,
                    "width"     : "10px",
                    "sClass"    : "text-center",
                    "orderable" : false
                },
                { 
                    data    : 'calibration_date',
                    name    : 'calibration_date'
                },
                { 
                    data    : 'input_hours',
                    name    : 'input_hours'
                },
                { 
                    data    : 'location',
                    name    : 'location'
                },
                { 
                    data    : 'temp',
                    name    : 'temp'
                },
                { 
                    data    : 'humidity',
                    name    : 'humidity'
                },
                { 
                    data    : 'action',
                    name    : 'action'
                },
            ],
            "order":[
                [1,'asc']
            ],
            "rowCallback": function(row, data, iDisplayIndex){
                var info    = this.fnPagingInfo();
                var page    = info.iPage;
                var lenght  = info.iLenght;
                var index   = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    }

    function delete_condition(id){
        swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },function(){
            $.ajax({
                type: "GET",
                url: "{{url('OffCenterTest/deletecondition?id=')}}"+id,
                dataType: "JSON",
                success: function (data) {
                    console.log(data)
                    swal("Deleted!", data.message, "success");
                    $('#condition_table').DataTable().ajax.reload();
                }         
            });
        });
    }
    // E: Condition
</script>
@endpush