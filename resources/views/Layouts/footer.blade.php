{{-- <!-- Jquery Core Js -->
<script src="{{asset('template/assets/plugins/jquery/jquery.min.js')}}"></script> --}}

<!-- Bootstrap Core Js -->
<script src="{{asset('template/assets/plugins/bootstrap/js/bootstrap.js')}}"></script>

<!-- Select Plugin Js -->
<script src="{{asset('template/assets/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{asset('template/assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="{{asset('template/assets/plugins/bootstrap-notify/bootstrap-notify.js')}}"></script>

<!-- Jquery Validation Plugin Css -->
<script src="{{asset('template/assets/plugins/jquery-validation/jquery.validate.js')}}"></script>

<!-- JQuery Steps Plugin Js -->
<script src="{{asset('template/assets/plugins/jquery-steps/jquery.steps.js')}}"></script>

<!-- Sweet Alert Plugin Js -->
<script src="{{asset('template/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{asset('template/assets/plugins/node-waves/waves.js')}}"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="{{asset('template/assets/plugins/jquery-countto/jquery.countTo.js')}}"></script>

<!-- Morris Plugin Js -->
<script src="{{asset('template/assets/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{asset('template/assets/plugins/morrisjs/morris.js')}}"></script>

<!-- ChartJs -->
<script src="{{asset('template/assets/plugins/chartjs/Chart.bundle.js')}}"></script>

{{-- <!-- Flot Charts Plugin Js -->
<script src="{{asset('template/assets/plugins/flot-charts/jquery.flot.js')}}"></script>
<script src="{{asset('template/assets/plugins/flot-charts/jquery.flot.resize.js')}}"></script>
<script src="{{asset('template/assets/plugins/flot-charts/jquery.flot.pie.js')}}"></script>
<script src="{{asset('template/assets/plugins/flot-charts/jquery.flot.categories.js')}}"></script>
<script src="{{asset('template/assets/plugins/flot-charts/jquery.flot.time.js')}}"></script> --}}

<!-- Sparkline Chart Plugin Js -->
<script src="{{asset('template/assets/plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('template/assets/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('template/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('template/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('template/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{asset('template/assets/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{asset('template/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{asset('template/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{asset('template/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{asset('template/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

<!-- SweetAlert Plugin Js -->
<script src="{{asset('template/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

<!-- Custom Js -->
<script src="{{asset('template/assets/js/admin.js')}}"></script>
<script src="{{asset('template/assets/js/pages/tables/jquery-datatable.js')}}"></script>
<script src="{{asset('template/assets/js/pages/forms/form-validation.js')}}"></script>
<script src="{{asset('template/assets/js/pages/ui/notifications.js')}}"></script>
{{-- <script src="{{asset('template/assets/js/pages/index.js')}}"></script> --}}

<!-- Notify Js -->
<script src="{{asset('template/assets/notify/notify.js')}}"></script>
<script src="{{asset('template/assets/notify/notify.min.js')}}"></script>

<!-- Demo Js -->
<script src="{{asset('template/assets/js/demo.js')}}"></script>
</body>

</html>