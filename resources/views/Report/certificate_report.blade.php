<style>
    table{
        font-size: 8px;
    }

    span{
        font-size: 10px;
    }
</style>

<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
        <tr>
            <td style="width: 72%;">PT. EPSON BATAM</td>
            <td style="width: 12%;">Certificate No</td>
            <td style="width: 1%;">:</td>
            <td style="width: 12%;">PEB/2019/BAL/0001</td>
        </tr>
        <tr>
            <td style="width: 72%;">TQC DEPARTMENT</td>
            <td style="width: 12%;">Date of Calibration</td>
            <td style="width: 1%;">:</td>
            <td style="width: 12%;">02 January 2020</td>
        </tr>
    </tbody>
</table>
<p style="text-align: center;font-size:12px;"><b>CERTIFICATE OF INTERNAL CALIBRATION</b></p>
<span style="text-align: justify;"><u><strong>Instrument Identity</strong></u></span>
<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
        <tr>
            <td style="width: 15%;">Instrument Name</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;">AND GF-350</td>
            <td style="width: 10%;">Location</td>
            <td style="width: 1%;">:</td>
            <td style="width: 13%;">IK Lab</td>
        </tr>
        <tr>
            <td style="width: 15%;">Manufacturer</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;"></td>
            <td style="width: 10%;">Temp</td>
            <td style="width: 1%;">:</td>
            <td style="width: 13%;">22.3</td>
        </tr>
        <tr>
            <td style="width: 15%;">Model No.</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;"></td>
            <td style="width: 10%;">Humidity</td>
            <td style="width: 1%;">:</td>
            <td style="width: 13%;">0.698</td>
        </tr>
        <tr>
            <td style="width: 15%;">Serial No.</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;"></td>
            <td style="width: 10%;">Calibration</td>
            <td style="width: 1%;">:</td>
            <td style="width: 13%;">02 January 2020</td>
        </tr>
        <tr>
            <td style="width: 15%;">Max Capacity</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;"></td>
            <td style="width: 10%;"></td>
            <td style="width: 1%;"></td>
            <td style="width: 13%;"></td>
        </tr>
        <tr>
            <td style="width: 15%;">Resolution</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;"></td>
            <td style="width: 10%;"></td>
            <td style="width: 1%;"></td>
            <td style="width: 13%;"></td>
        </tr>
        <tr>
            <td style="width: 15%;">Type</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;"></td>
            <td style="width: 10%;"></td>
            <td style="width: 1%;"></td>
            <td style="width: 13%;"></td>
        </tr>
    </tbody>
</table>
<span style="text-align: justify;"><u><strong>Calibration Reference Used and Traceability</strong></u></span>
<table style="border-collapse: collapse; width: 100%;" border="1">
    <tbody>
        <tr>
            <td style="width: 15%; text-align: center;">Standard Weight</td>
            <td style="width: 17%; text-align: center;">Tag No</td>
            <td style="width: 17%; text-align: center;">Serial No</td>
            <td style="width: 17%; text-align: center;">Report No</td>
            <td style="width: 17%; text-align: center;">Cal. Due Date</td>
            <td style="width: 17%; text-align: center;">Uncertainly (U95)</td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center;">1</td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center;">5</td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center;">10</td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center;">15</td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center;">100</td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center;">200</td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center;">300</td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center;">400</td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center;">500</td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
            <td style="width: 17%; text-align: center;"></td>
        </tr>
    </tbody>
</table>
<span style="text-align: justify;"><u><strong>Result Calibration</strong></u></span><br>
<span><strong>1. Off Center Test</strong></span>
<table style="border-collapse: collapse; width: 100%;" border="1">
    <tbody>
        <tr>
            <td style="width: 20%; text-align: center;">Weight</td>
            <td style="width: 20%; text-align: center;" colspan="4">100</td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: center;" rowspan="2">Position</td>
            <td style="width: 20%; text-align: center;" colspan="4">Balance Reading (gr)</td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: center;">AVG</td>
            <td style="width: 20%; text-align: center;">Error</td>
            <td style="width: 20%; text-align: center;">%</td>
            <td style="width: 20%; text-align: center;">Correction</td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: center;">Max</td>
            <td style="width: 20%; text-align: center;"></td>
            <td style="width: 20%; text-align: center;"></td>
            <td style="width: 20%; text-align: center;"></td>
            <td style="width: 20%; text-align: center;"></td>
        </tr>
    </tbody>
</table>
<span><strong>2. Repeatability Test</strong></span>
<table style="border-collapse: collapse; width: 100%;" border="1">
    <tbody>
        <tr>
            <td style="width: 14.2857%; text-align: center;" colspan="2">Weight Standard</td>
            <td style="width: 14.2857%; text-align: center;" colspan="3">Balance Reading (gr)</td>
            <td style="width: 14.2857%; text-align: center;" rowspan="2">Correction</td>
            <td style="width: 14.2857%; text-align: center;" rowspan="2">Uncertainly U95 *)</td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">% Cap</td>
            <td style="width: 14.2857%; text-align: center;">Value (gr)</td>
            <td style="width: 14.2857%; text-align: center;">Avg (gr)</td>
            <td style="width: 14.2857%; text-align: center;">Error</td>
            <td style="width: 14.2857%; text-align: center;">Error (%)</td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">10%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">20%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">30%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">40%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">50%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">60%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">70%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">80%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">90%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;">100%</td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
            <td style="width: 14.2857%; text-align: center;"></td>
        </tr>
    </tbody>
</table>
<span>*) Acceptable Uncertainly standards U95 < tolerance measuring</span><br><br>
<span style="text-align: justify;"><u><strong>Conclusion</strong></u></span><br>
<span><strong>Based on result calibration above, the equipment is still acceptable to use.</strong></span>
<table style="width: 100%; border-collapse: collapse;">
    <tbody>
        <tr>
            <td style="width: 42.6825%; height:70px; border-color: black; border-style: solid; text-align: left; vertical-align: top;" rowspan="5">Note:</td>
            <td style="width: 30.7186%;" rowspan="5"></td>
            <td style="width: 13.1866%; text-align: center; border-style: solid; border-color: black;">Prepared By,</td>
            <td style="width: 13.4124%; text-align: center; border-style: solid; border-color: black;">Aproved By,</td>
        </tr>
        <tr>
            <td style="width: 13.1866%; text-align: center; border-style: solid; border-color: black;" rowspan="3"></td>
            <td style="width: 13.4124%; text-align: center; border-style: solid; border-color: black;" rowspan="3"></td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td style="width: 13.1866%; text-align: center; border-style: solid; border-color: black;">Alfie Sy.</td>
            <td style="width: 13.4124%; text-align: center; border-style: solid; border-color: black;">Gandhi S.</td>
        </tr>
    </tbody>
</table>