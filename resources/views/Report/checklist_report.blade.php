<style>
    table{
        font-size: 8px;
    }

    span{
        font-size: 10px;
    }
</style>

<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
        <tr><td style="width: 72%;">PT. EPSON BATAM</td></tr>
        <tr><td style="width: 72%;">TQC DEPARTMENT</td></tr>
    </tbody>
</table>
<p style="text-align: center;font-size:12px;"><b>CHECKLIST INTERNAL CALIBRATION</b></p>
<span style="text-align: justify;"><u><strong>Instrument Identity</strong></u></span>
<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
        <tr>
            <td style="width: 15%;">Instrument Name</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;">{{$data->getInstrument->instrument_name}}</td>
            <td style="width: 10%;">Location</td>
            <td style="width: 1%;">:</td>
            <td style="width: 13%;">{{$data->location}}</td>
        </tr>
        <tr>
            <td style="width: 15%;">Manufacturer</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;">{{$data->getInstrument->manufacturer}}</td>
            <td style="width: 10%;">Temp</td>
            <td style="width: 1%;">:</td>
            <td style="width: 13%;">{{$data->temp}}</td>
        </tr>
        <tr>
            <td style="width: 15%;">Model No.</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;">{{$data->getInstrument->model_no}}</td>
            <td style="width: 10%;">Humidity</td>
            <td style="width: 1%;">:</td>
            <td style="width: 13%;">{{$data->humidity}}</td>
        </tr>
        <tr>
            <td style="width: 15%;">Serial No.</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;">{{$data->getInstrument->serial_no}}</td>
            <td style="width: 10%;">Calibration</td>
            <td style="width: 1%;">:</td>
            <td style="width: 13%;">{{date("d M Y",strtotime($data->created_at))}}</td>
        </tr>
        <tr>
            <td style="width: 15%;">Max Capacity</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;">{{$data->getInstrument->max_capacity}}</td>
            <td style="width: 10%;"></td>
            <td style="width: 1%;"></td>
            <td style="width: 13%;"></td>
        </tr>
        <tr>
            <td style="width: 15%;">Resolution</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;">{{$data->getInstrument->resolution}}</td>
            <td style="width: 10%;"></td>
            <td style="width: 1%;"></td>
            <td style="width: 13%;"></td>
        </tr>
        <tr>
            <td style="width: 15%;">Type</td>
            <td style="width: 1%;">:</td>
            <td style="width: 60%;">{{$data->getInstrument->type}}</td>
            <td style="width: 10%;"></td>
            <td style="width: 1%;"></td>
            <td style="width: 13%;"></td>
        </tr>
    </tbody>
</table>

<span><strong>1. Off Center Test</strong></span>
<table style="border-collapse: collapse; width: 100%;" border="1">
    <tbody>
        <tr>
            <td style="width: 20%; text-align: center;"><b>Weight</b></td>
            <td style="width: 20%; text-align: center;" colspan="4">{{$data->getOct->weight_standard}}</td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: center;" rowspan="2"><b>Position</b></td>
            <td style="width: 20%; text-align: center;" colspan="4"><b>Balance Reading (gr)</b></td>
        </tr>
        <tr>
            <td style="width: 20%; text-align: center;"><b>1st</b></td>
            <td style="width: 20%; text-align: center;"><b>2nd</b></td>
            <td style="width: 20%; text-align: center;"><b>3rd</b></td>
            <td style="width: 20%; text-align: center;"><b>Average</b></td>
        </tr>
        @foreach ($oct as $items)
            <tr>
                <td style="width: 20%; text-align: center;">{{$items['getPosition']['position']}}</td>
                <td style="width: 20%; text-align: center;">{{$items['br1']}}</td>
                <td style="width: 20%; text-align: center;">{{$items['br2']}}</td>
                <td style="width: 20%; text-align: center;">{{$items['br3']}}</td>
                <td style="width: 20%; text-align: center;">{{$items['avg']}}</td>
            </tr>
        @endforeach
        <tr>
            <td style="width: 20%; text-align: center;"><b>Max</b></td>
            <td style="width: 20%; text-align: center;"><b>{{$max['br1']}}</b></td>
            <td style="width: 20%; text-align: center;"><b>{{$max['br2']}}</b></td>
            <td style="width: 20%; text-align: center;"><b>{{$max['br3']}}</b></td>
            <td style="width: 20%; text-align: center;"><b>{{$max['avg']}}</b></td>
        </tr>
    </tbody>
</table>
<span><strong>2. Repeatability Test</strong></span>
<table style="border-collapse: collapse; width: 100%;" border="1">
    <tbody>
        <tr>
            <td style="width: 14.2857%; text-align: center;" colspan="2"><b>Weight Standard</b></td>
            <td style="width: 14.2857%; text-align: center;" rowspan="2"><b>Uncertainly U95</b></td>
            <td style="width: 14.2857%; text-align: center;" colspan="4"><b>Balance Reading (gr)</b></td>
        </tr>
        <tr>
            <td style="width: 14.2857%; text-align: center;"><b>% Cap</b></td>
            <td style="width: 14.2857%; text-align: center;"><b>Value (gr)</b></td>
            <td style="width: 14.2857%; text-align: center;"><b>1st</b></td>
            <td style="width: 14.2857%; text-align: center;"><b>2nd</b></td>
            <td style="width: 14.2857%; text-align: center;"><b>3rd</b></td>
            <td style="width: 14.2857%; text-align: center;"><b>StDev</b></td>
        </tr>
        @foreach ($rt as $item)
            <tr>
                <td style="width: 14.2857%; text-align: center;">{{$item['cap']}}</td>
                <td style="width: 14.2857%; text-align: center;">{{$item['value']}}</td>
                <td style="width: 14.2857%; text-align: center;">{{$item['u95_cs']}}</td>
                <td style="width: 14.2857%; text-align: center;">{{$item['br1']}}</td>
                <td style="width: 14.2857%; text-align: center;">{{$item['br2']}}</td>
                <td style="width: 14.2857%; text-align: center;">{{$item['br3']}}</td>
                <td style="width: 14.2857%; text-align: center;">{{$item['stdev']}}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<p></p>
<table style="width: 100%; border-collapse: collapse;" border="">
<tbody>
<tr>
<td style="width: 10%;"></td>
<td style="width: 30%;" colspan="2"><strong><u>Repeatability</u></strong></td>
<td style="width: 10%;"></td>
<td style="width: 30%;" colspan="2"><strong><u>Standard Certificate</u></strong></td>
</tr>
<tr>
<td style="width: 10%;"></td>
<td style="width: 15%;">StdDev max</td>
<td style="width: 15%;">: {{$data_U95['stdevmax']}}</td>
<td style="width: 10%;"></td>
<td style="width: 15%;">Ustandard</td>
<td style="width: 15%;">: {{$data_U95['ustandard']}}</td>
</tr>
<tr>
<td style="width: 10%;"></td>
<td style="width: 15%;">n</td>
<td style="width: 15%;">: {{$data_U95['n']}}</td>
<td style="width: 10%;"></td>
<td style="width: 15%;">k -standard</td>
<td style="width: 15%;">: {{$data_U95['kstandard']}}</td>
</tr>
<tr>
<td style="width: 10%;"></td>
<td style="width: 15%;">Ua</td>
<td style="width: 15%;">: {{$data_U95['ua']}}</td>
<td style="width: 10%;"></td>
<td style="width: 15%;">Ucertificate</td>
<td style="width: 15%;">: {{$data_U95['ucertificate']}}</td>
</tr>
<tr>
<td style="width: 10%;"></td>
<td style="width: 15%;">Va</td>
<td style="width: 15%;">: {{$data_U95['va']}}</td>
<td style="width: 10%;"></td>
<td style="width: 15%;">Vcertificate</td>
<td style="width: 15%;">: {{$data_U95['vcertificate']}}</td>
</tr>
</tbody>
</table>
<p></p>
<table style="width: 100%; border-collapse: collapse;" border="">
<tbody>
<tr>
<td style="width: 10%;"></td>
<td style="width: 30%;" colspan="2"><strong><u>Readability</u></strong></td>
<td style="width: 10%;"></td>
<td style="width: 30%;" colspan="2"><strong><u>Uncertainly</u></strong></td>
</tr>
<tr>
<td style="width: 10%;"></td>
<td style="width: 15%;">Res</td>
<td style="width: 15%;">: {{$data_U95['res']}}</td>
<td style="width: 10%;"></td>
<td style="width: 15%;">Uc</td>
<td style="width: 15%;">: {{$data_U95['uc']}}</td>
</tr>
<tr>
<td style="width: 10%;"></td>
<td style="width: 15%;">Dist. Rect</td>
<td style="width: 15%;">: {{$data_U95['distrect']}}</td>
<td style="width: 10%;"></td>
<td style="width: 15%;">Veff</td>
<td style="width: 15%;">: {{$data_U95['veff']}}</td>
</tr>
<tr>
<td style="width: 10%;"></td>
<td style="width: 15%;">Ures</td>
<td style="width: 15%;">: {{$data_U95['ures']}}</td>
<td style="width: 10%;"></td>
<td style="width: 15%;">k -eff</td>
<td style="width: 15%;">: {{$data_U95['keff']}}</td>
</tr>
<tr>
<td style="width: 10%;"></td>
<td style="width: 15%;">Vres</td>
<td style="width: 15%;">: {{$data_U95['vres']}}</td>
<td style="width: 10%;"></td>
<td style="width: 15%;">U95</td>
<td style="width: 15%;">: {{$data_U95['u95']}}</td>
</tr>
</tbody>
</table>
<p></p>

<table style="width: 100%; border-collapse: collapse;">
    <tbody>
        <tr>
            <td style="width: 40%; height:70px;" rowspan="5"></td>
            <td style="width: 30%;" rowspan="5"></td>
            <td style="width: 15%; text-align: center; border-style: solid; border-color: black;">Prepared By,</td>
            <td style="width: 15%; text-align: center; border-style: solid; border-color: black;">Aproved By,</td>
        </tr>
        <tr>
            <td style="width: 15%; text-align: center; border-style: solid; border-color: black;" rowspan="3"></td>
            <td style="width: 15%; text-align: center; border-style: solid; border-color: black;" rowspan="3"></td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td style="width: 15%; text-align: center; border-style: solid; border-color: black;">{{$data->getUserPrepare->name}}</td>
            <td style="width: 15%; text-align: center; border-style: solid; border-color: black;">{{$data->getUserApprove->name}}</td>
        </tr>
    </tbody>
</table>