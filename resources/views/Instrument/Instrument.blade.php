@extends('index')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Instrument
                {{-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> --}}
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        {{-- <h2>
                            Users Data
                        </h2> --}}
                        <button onclick="add_instrument()" id="btn_add_instrument" type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#modal_add_instrument">
                            <i class="material-icons">add_circle</i>
                            <span>Add Instrument</span>
                        </button>
                        {{-- <button type="btn_add_user" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#modal_add_user">
                            MODAL - DEFAULT SIZE</button> --}}
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="instrument_table" cellspacing="0" width="100%" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Instrument Name</th>
                                        <th>Manufacturer</th>
                                        <th>Model No</th>
                                        <th>Serial No</th>
                                        <th>Max Capacity</th>
                                        <th>Resolution</th>
                                        <th>Type</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

        <div class="modal fade" id="modal_add_instrument" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Form Instrument</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_instrument" name="form_instrument">
                            @csrf
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <select class="form-control show-tick" data-live-search="true" id="select_category" name="select_category">
                                        <option>Select Category</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="instrument_name" name="instrument_name" required>
                                    <label class="form-label">Instrument Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="manufacturer" name="manufacturer" required>
                                    <label class="form-label">Manufacturer</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="model_no" name="model_no" required>
                                    <label class="form-label">Model No</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="serial_no" name="serial_no" required>
                                    <label class="form-label">Serial No</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="max_capacity" name="max_capacity" required>
                                    <label class="form-label">Max Capacity</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="resolution" name="resolution" required>
                                    <label class="form-label">Resolution</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="type" name="type" required>
                                    <label class="form-label">Type</label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button onClick="simpan_instrument()" type="button" id="btn_simpan_instrument" class="btn btn-link waves-effect">SAVE</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function() {
        instrument_table();
        getCategory();
        // condition_table();
    })

    function getCategory(){
        $.ajax({
            url:'{{route("get-category")}}',
            type:'GET',
            dataType:'json',
            async:true,
            success:function(data) { 
                var options;
                for (var i = 0; i < data['length']; i++) {
                    options += "<option value='"+data[i].id+"'>"+data[i].category_name+"</option>";
                }
                $("#select_category").append(options);
                $('#select_category').selectpicker('refresh');
            }
        });
    }

    function instrument_table(){
        url = '{{route("show-instrument")}}'
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return{
                "iStart"        : oSettings._iDisplayStart,
                "iEnd"          : oSettings.fnDisplayEnd(),
                "iLenght"       : oSettings._iDisplayLength,
                "iTotal"        : oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage"         : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages"   : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            }
        }

        var table = $('#instrument_table').DataTable({
            processing  : true,
            serverSide  : false,
            bDestroy    : true,
            ajax        : url,
            columns     : [
                {
                    "data"      : null,
                    "width"     : "10px",
                    "sClass"    : "text-center",
                    "orderable" : false
                },
                { 
                    data    : 'instrument_name',
                    name    : 'instrument_name'
                },
                { 
                    data    : 'manufacturer',
                    name    : 'manufacturer'
                },
                { 
                    data    : 'model_no',
                    name    : 'model_no'
                },
                { 
                    data    : 'serial_no',
                    name    : 'serial_no'
                },
                { 
                    data    : 'max_capacity',
                    name    : 'max_capacity'
                },
                { 
                    data    : 'resolution',
                    name    : 'resolution'
                },
                { 
                    data    : 'type',
                    name    : 'type'
                },
                { 
                    data    : 'action',
                    name    : 'action'
                },
            ],
            "order":[
                [1,'asc']
            ],
            "rowCallback": function(row, data, iDisplayIndex){
                var info    = this.fnPagingInfo();
                var page    = info.iPage;
                var lenght  = info.iLenght;
                var index   = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    }
    
    // S: instrument 

    var save_method;
    var id_instrument;
    function add_instrument(id, sv_mtd){
        id_instrument = id;
        save_method = sv_mtd
        
        if(save_method == 'edit'){
            $.ajax({
                url : "{{url('Instrument/showedit?id=')}}"+id_instrument,
                type : "GET",
                dataType: "JSON",
                async:true,
                success:function(data){
                    console.log(data)
                    $('#instrument_name').val(data.instrument_name)
                    $('#manufacturer').val(data.manufacturer)
                    $('#model_no').val(data.model_no)
                    $('#serial_no').val(data.serial_no)
                    $('#max_capacity').val(data.max_capacity)
                    $('#resolution').val(data.resolution)
                    $('#type').val(data.type)
                    // $('#select_category').text(data.id_category);
                    // $( "#select_category option:selected" ).text(data.id_category);
                }
            })
        }else{
            $('#instrument_name').val('')
            $('#manufacturer').val('')
            $('#model_no').val('')
            $('#serial_no').val('')
            $('#max_capacity').val('')
            $('#resolution').val('')
            $('#type').val('')
            // $('#select_category').text('');
            // $( "#select_category option:selected" ).text();
        }
    }

    
    
    function simpan_instrument(){
        // add_instrument(id_instrument,save_method)
        console.log(id_instrument)
        console.log(save_method)
        var form_data = $('#form_instrument').serialize();
        if (save_method == 'edit'){
            var url = "{{url('Instrument/post?id=')}}"+id_instrument;
        }else{
            var url = "{{url('Instrument/post')}}"
        }
        console.log(form_data)
        $.ajax({
            url     : url,
            type    : 'POST',
            data    : form_data,
            async   : false,
            success : function(data){
                console.log(data)
                if(data.status = "success"){
                    $("#form_instrument")[0].reset();
                    $('#instrument_table').DataTable().ajax.reload();
                    $('#modal_add_instrument').modal('hide')
                    $.notify(data.message, "success");
                }else{
                    console.log('das')
                    $("#form_instrument")[0].reset();
                    $('#instrument_table').DataTable().ajax.reload();
                    $.notify(data.message, "danger");
                }
            },
            error: function(data) {
                console.log('adsllk')
                $.notify(data.message, "danger");
            }
        });
    }

    function delete_instrument(id){
        swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },function(){
            $.ajax({
                type: "GET",
                url: "{{url('Instrument/delete?id=')}}"+id,
                dataType: "JSON",
                success: function (data) {
                    console.log(data)
                    swal("Deleted!", data.message, "success");
                    $('#instrument_table').DataTable().ajax.reload();
                }         
            });
        });
    }
    // E: Instrument


    
</script>
@endpush