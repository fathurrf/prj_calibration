@extends('index')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                User Management
                {{-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> --}}
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        {{-- <h2>
                            Users Data
                        </h2> --}}
                        <button onclick="add_user()" id="btn_add_user" type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#modal_add_user">
                            <i class="material-icons">add_circle</i>
                            <span>Add User</span>
                        </button>
                        {{-- <button type="btn_add_user" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#modal_add_user">
                            MODAL - DEFAULT SIZE</button> --}}
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="users_table" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Username</th>
                                        <th>Department</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                {{-- <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Username</th>
                                        <th>Department</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot> --}}
                                {{-- <tbody> --}}
                                    {{-- @foreach ($viewUser as $item) --}}
                                        {{-- <tr> --}}
                                            {{-- <td class="text-center">{{$loop->iteration}}</td> --}}
                                            {{-- <td>{{$item->name}}</td> --}}
                                            {{-- <td>{{$item->email}}</td> --}}
                                            {{-- <td>{{$item->username}}</td> --}}
                                            {{-- <td>{{$item->role}}</td> --}}
                                            {{-- <td> --}}
                                                {{-- <a id="detail" href="#" class=""><span class="badge bg-green">Detail</span></a> --}}
                                                {{-- <a href="#" class=""><span class="badge bg-orange">Edit</span></a> --}}
                                                {{-- <a href="#" class=""><span class="badge bg-red">Hapus</span></a> --}}
                                            {{-- </td> --}}
                                        {{-- </tr>     --}}
                                    {{-- @endforeach --}}
                                {{-- </tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

        <div class="modal fade" id="modal_add_user" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Form User</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_user" name="form_user">
                            @csrf
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <select class="form-control show-tick" data-live-search="true" id="select_department" name="select_department">
                                        <option>Select Department</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="nama" name="nama" required>
                                    <label class="form-label">Nama</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" class="form-control" id="email" name="email" required>
                                    <label class="form-label">Email</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" class="form-control" id="username" name="username" required>
                                    <label class="form-label">Username</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" class="form-control" id="password" name="password" required>
                                    <label class="form-label">Password</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <select class="form-control show-tick" data-live-search="true" id="select_role" name="select_role">
                                        <option>Select Role</option>
                                        <option value="super_user">Super User</option>
                                        <option value="manager">Manager</option>
                                        <option value="operator">Operator</option>
                                    </select>
                                </div>
                            </div>
                            
                            {{-- <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button> --}}
                            <div class="modal-footer">
                                <button onClick="simpan_user()" type="button" id="btn_simpan_user" class="btn btn-link waves-effect">SAVE</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                            </div>
                        </form>
                        <!-- #END# Basic Validation -->
                    </div>
                    {{-- <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div> --}}
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function() {
        users_table();
        getDepartment();
    })

    function getDepartment(){
        $.ajax({
            url:'{{route("get-department")}}',
            type:'GET',
            dataType:'json',
            async:true,
            success:function(data) { 
                var options;
                for (var i = 0; i < data['length']; i++) {
                    options += "<option value='"+data[i].id+"'>"+data[i].department_name+"</option>";
                }
                $("#select_department").append(options);
                $('#select_department').selectpicker('refresh');
            }
        });
    }

    function users_table(){
        url = '{{route("show-user")}}'
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return{
                "iStart"        : oSettings._iDisplayStart,
                "iEnd"          : oSettings.fnDisplayEnd(),
                "iLenght"       : oSettings._iDisplayLength,
                "iTotal"        : oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage"         : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages"   : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            }
        }

        var table = $('#users_table').DataTable({
            processing  : true,
            serverSide  : false,
            bDestroy    : true,
            ajax        : url,
            columns     : [
                {
                    "data"      : null,
                    "width"     : "10px",
                    "sClass"    : "text-center",
                    "orderable" : false
                },
                { 
                    data    : 'name',
                    name    : 'name'
                },
                { 
                    data    : 'email',
                    name    : 'email'
                },
                { 
                    data    : 'username',
                    name    : 'username'
                },
                { 
                    data    : 'get_department.department_name',
                    name    : 'department_name',
                    defaultContent     : 'kosong' 
                },
                { 
                    data    : 'role',
                    name    : 'role'
                },
                { 
                    data    : 'action',
                    name    : 'action'
                },
            ],
            "order":[
                [1,'asc']
            ],
            "rowCallback": function(row, data, iDisplayIndex){
                var info    = this.fnPagingInfo();
                var page    = info.iPage;
                var lenght  = info.iLenght;
                var index   = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    }
    
    // $("#btn_add_user").on("click", function(){
    //     // $("#modal_add_user").show()
    //     // console.log('dasd')
    // })
    var save_method;
    var id_user;
    function add_user(id, sv_mtd){
        id_user = id;
        save_method = sv_mtd
        
        if(save_method == 'edit'){
            $.ajax({
                url : "{{url('UserManagement/showedit?id=')}}"+id_user,
                type : "GET",
                dataType: "JSON",
                async:true,
                success:function(data){
                    console.log(data)
                    $('#nama').val(data.name)
                    $('#email').val(data.email)
                    $('#username').val(data.username)
                }
            })
        }else{
            $('#nama').val()
            $('#email').val()
            $('#username').val()
        }
    }
    
    function simpan_user(){
        add_user(id_user,save_method)
        console.log(id_user)
        console.log(save_method)
        var form_data = $('#form_user').serialize();
        if (save_method == 'edit'){
            var url = "{{url('UserManagement/postuser?id=')}}"+id_user;
        }else{
            var url = "{{url('UserManagement/postuser')}}"
        }
        console.log(form_data)
        $.ajax({
            url     : url,
            type    : 'POST',
            data    : form_data,
            async   : false,
            success : function(data){
                console.log(data)
                if(data.status = "success"){
                    $("#form_user")[0].reset();
                    $('#users_table').DataTable().ajax.reload();
                    $('#modal_add_user').modal('hide')
                    $.notify(data.message, "success");
                }else{
                    console.log('das')
                    $("#form_user")[0].reset();
                    $('#users_table').DataTable().ajax.reload();
                    $.notify(data.message, "danger");
                }
            },
            error: function(data) {
                console.log('adsllk')
                $.notify(data.message, "danger");
            }
        });
    }

    function delete_user(id){
        swal({
            // title: "Are you sure!",
            // type: "error",
            // confirmButtonClass: "btn-danger",
            // confirmButtonText: "Yes!",
            // showCancelButton: true,
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },function(){
            $.ajax({
                type: "GET",
                url: "{{url('UserManagement/deleteuser?id=')}}"+id,
                dataType: "JSON",
                success: function (data) {
                    console.log(data)
                    swal("Deleted!", data.message, "success");
                    $('#users_table').DataTable().ajax.reload();
                }         
            });
        });
    }
</script>
@endpush