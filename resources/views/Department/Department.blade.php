@extends('index')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Department
                {{-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> --}}
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        {{-- <h2>
                            Users Data
                        </h2> --}}
                        <button onclick="add_department()" id="btn_add_department" type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#modal_add_department">
                            <i class="material-icons">add_circle</i>
                            <span>Add Department</span>
                        </button>
                        {{-- <button type="btn_add_user" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#modal_add_user">
                            MODAL - DEFAULT SIZE</button> --}}
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="department_table" cellspacing="0" width="100%" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Department Name</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

        <div class="modal fade" id="modal_add_department" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Form Department</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_department" name="form_department">
                            @csrf
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="department_name" name="department_name" required>
                                    <label class="form-label">Department Name</label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button onClick="simpan_department()" type="button" id="btn_simpan_department" class="btn btn-link waves-effect">SAVE</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function() {
        department_table();
        // condition_table();
    })

    function department_table(){
        url = '{{route("show-department")}}'
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return{
                "iStart"        : oSettings._iDisplayStart,
                "iEnd"          : oSettings.fnDisplayEnd(),
                "iLenght"       : oSettings._iDisplayLength,
                "iTotal"        : oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage"         : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages"   : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            }
        }

        var table = $('#department_table').DataTable({
            processing  : true,
            serverSide  : false,
            bDestroy    : true,
            ajax        : url,
            columns     : [
                {
                    "data"      : null,
                    "width"     : "10px",
                    "sClass"    : "text-center",
                    "orderable" : false
                },
                { 
                    data    : 'department_name',
                    name    : 'department_name'
                },
                { 
                    data    : 'action',
                    name    : 'action'
                },
            ],
            "order":[
                [1,'asc']
            ],
            "rowCallback": function(row, data, iDisplayIndex){
                var info    = this.fnPagingInfo();
                var page    = info.iPage;
                var lenght  = info.iLenght;
                var index   = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    }
    
    // S: department 

    var save_method;
    var id_department;
    function add_department(id, sv_mtd){
        id_department = id;
        save_method = sv_mtd
        
        if(save_method == 'edit'){
            $.ajax({
                url : "{{url('Department/showedit?id=')}}"+id_department,
                type : "GET",
                dataType: "JSON",
                async:true,
                success:function(data){
                    console.log(data)
                    $('#department_name').val(data.department_name)
                }
            })
        }else{
            $('#department_name').val('')
        }
    }
    
    function simpan_department(){
        // add_department(id_department,save_method)
        console.log(id_department)
        console.log(save_method)
        var form_data = $('#form_department').serialize();
        if (save_method == 'edit'){
            var url = "{{url('Department/post?id=')}}"+id_department;
        }else{
            var url = "{{url('Department/post')}}"
        }
        console.log(form_data)
        $.ajax({
            url     : url,
            type    : 'POST',
            data    : form_data,
            async   : false,
            success : function(data){
                console.log(data)
                if(data.status = "success"){
                    $("#form_department")[0].reset();
                    $('#department_table').DataTable().ajax.reload();
                    $('#modal_add_department').modal('hide')
                    $.notify(data.message, "success");
                }else{
                    console.log('das')
                    $("#form_department")[0].reset();
                    $('#department_table').DataTable().ajax.reload();
                    $.notify(data.message, "danger");
                }
            },
            error: function(data) {
                console.log('adsllk')
                $.notify(data.message, "danger");
            }
        });
    }

    function delete_department(id){
        swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },function(){
            $.ajax({
                type: "GET",
                url: "{{url('Department/delete?id=')}}"+id,
                dataType: "JSON",
                success: function (data) {
                    console.log(data)
                    swal("Deleted!", data.message, "success");
                    $('#department_table').DataTable().ajax.reload();
                }         
            });
        });
    }
    // E: Department

</script>
@endpush