<?php
Route::group(['prefix' => 'UserManagement'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::GET('/', 'Calibration\UserController@index')->name('index-user');
        Route::GET('show', 'Calibration\UserController@show')->name('show-user');
        Route::POST('postuser', 'Calibration\UserController@postUser')->name('post-user');
        Route::GET('showedit', 'Calibration\UserController@showEdit')->name('show-edit');
        Route::GET('deleteuser', 'Calibration\UserController@deleteUser')->name('delete-user');
        Route::GET('getdepartment', 'Calibration\UserController@getDepartment')->name('get-department');
    });
});