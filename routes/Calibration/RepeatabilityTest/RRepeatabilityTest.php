<?php

Route::group(['prefix' => 'RepeatabilityTest'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::GET('/', 'Calibration\RepeatabilityTestController@index')->name('index-rt');
    });
});