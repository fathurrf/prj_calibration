<?php
Route::group(['prefix' => 'OffCenterTest'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::GET('/', 'Calibration\OffCenterTestController@index')->name('index-oct');
        Route::GET('show', 'Calibration\OffCenterTestController@show')->name('show-oct');
        Route::GET('getposition', 'Calibration\OffCenterTestController@getPosition')->name('get-position');
        Route::GET('getcap', 'Calibration\OffCenterTestController@getCap')->name('get-cap');
        Route::GET('showdetailoct', 'Calibration\OffCenterTestController@showDetailOct')->name('show-detail-oct');
        Route::GET('showdetailrt', 'Calibration\OffCenterTestController@showDetailRt')->name('show-detail-rt');
        Route::POST('postoct', 'Calibration\OffCenterTestController@postOct')->name('post-oct');
        Route::GET('getdetailoct', 'Calibration\OffCenterTestController@getDetailOct')->name('get-detail-oct');
        Route::GET('getdetailrt', 'Calibration\OffCenterTestController@getDetailRt')->name('get-detail-rt');
        Route::GET('deleteoct', 'Calibration\OffCenterTestController@deleteOct')->name('delete-oct');
        Route::GET('prepareoct', 'Calibration\OffCenterTestController@prepareOct')->name('prepare-oct');
        Route::GET('approveoct', 'Calibration\OffCenterTestController@approveOct')->name('approve-oct');
        Route::GET('certificatereport', 'Calibration\OffCenterTestController@certificateReport')->name('certificate-report');
        Route::GET('checklistreport', 'Calibration\OffCenterTestController@checklistReport')->name('checklist-report');
        // Route::GET('showedit', 'Calibration\OffCenterTestController@showEdit')->name('show-edit-oct');
        // Route::GET('delete', 'Calibration\OffCenterTestController@delete')->name('delete-oct');
        // Route::POST('postcondition', 'Calibration\OffCenterTestController@postCondition')->name('post-condition');
        // Route::GET('showcondition', 'Calibration\OffCenterTestController@showCondition')->name('show-condition');
        // Route::GET('detailcondition', 'Calibration\OffCenterTestController@detailCondition')->name('detail-condition');
        // Route::GET('deletecondition', 'Calibration\OffCenterTestController@deleteCondition')->name('delete-condition');
    });
});