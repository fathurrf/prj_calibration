<?php
Route::group(['prefix' => 'DetailInstrument'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::GET('/', 'Calibration\DetailInstrumentController@index')->name('index-oct');
        Route::GET('show', 'Calibration\DetailInstrumentController@show')->name('show-oct');
        Route::POST('post', 'Calibration\DetailInstrumentController@post')->name('post-oct');
        Route::GET('showedit', 'Calibration\DetailInstrumentController@showEdit')->name('show-edit-oct');
        Route::GET('delete', 'Calibration\DetailInstrumentController@delete')->name('delete-oct');
        Route::POST('postcondition', 'Calibration\DetailInstrumentController@postCondition')->name('post-condition');
        Route::GET('showcondition', 'Calibration\DetailInstrumentController@showCondition')->name('show-condition');
        Route::GET('detailcondition', 'Calibration\DetailInstrumentController@detailCondition')->name('detail-condition');
        Route::GET('deletecondition', 'Calibration\DetailInstrumentController@deleteCondition')->name('delete-condition');
    });
});