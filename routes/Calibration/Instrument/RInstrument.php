<?php
Route::group(['prefix' => 'Instrument'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::GET('/', 'Calibration\InstrumentController@index')->name('index-instrument');
        Route::GET('show', 'Calibration\InstrumentController@show')->name('show-instrument');
        Route::POST('post', 'Calibration\InstrumentController@post')->name('post-instrument');
        Route::GET('showedit', 'Calibration\InstrumentController@showEdit')->name('show-edit-instrument');
        Route::GET('delete', 'Calibration\InstrumentController@delete')->name('delete-instrument');
        Route::GET('getcategory', 'Calibration\InstrumentController@getCategory')->name('get-category');
        // Route::POST('postcondition', 'Calibration\InstrumentController@postCondition')->name('post-condition');
        // Route::GET('showcondition', 'Calibration\InstrumentController@showCondition')->name('show-condition');
        // Route::GET('detailcondition', 'Calibration\InstrumentController@detailCondition')->name('detail-condition');
        // Route::GET('deletecondition', 'Calibration\InstrumentController@deleteCondition')->name('delete-condition');
    });
});