<?php
Route::group(['prefix' => 'Category'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::GET('/', 'Calibration\CategoryController@index')->name('index-category');
        Route::GET('show', 'Calibration\CategoryController@show')->name('show-category');
        Route::POST('post', 'Calibration\CategoryController@post')->name('post-category');
        Route::GET('showedit', 'Calibration\CategoryController@showEdit')->name('show-edit-category');
        Route::GET('delete', 'Calibration\CategoryController@delete')->name('delete-category');
    });
});