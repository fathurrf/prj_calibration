<?php
Route::group(['prefix' => 'Department'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::GET('/', 'Calibration\DepartmentController@index')->name('index-department');
        Route::GET('show', 'Calibration\DepartmentController@show')->name('show-department');
        Route::POST('post', 'Calibration\DepartmentController@post')->name('post-department');
        Route::GET('showedit', 'Calibration\DepartmentController@showEdit')->name('show-edit-department');
        Route::GET('delete', 'Calibration\DepartmentController@delete')->name('delete-department');
    });
});