<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::GET('/home', 'HomeController@dashboard')->name('view-index')->middleware('auth');
Route::GET('notfound','Calibration\LoginController@notfound')->name('notfound')->middleware('auth');

require_once 'Calibration/UserManagement/RUserManagement.php';
require_once 'Calibration/Category/RCategory.php';
require_once 'Calibration/Department/RDepartment.php';
require_once 'Calibration/Instrument/RInstrument.php';
require_once 'Calibration/Instrument/RDetailInstrument.php';
require_once 'Calibration/OffCenterTest/ROffCenterTest.php';
require_once 'Calibration/RepeatabilityTest/RRepeatabilityTest.php';

Route::group(['middleware' => 'guest'],function(){
    Route::GET('',function(){
        return redirect('login');
    });
    Route::GET('login', 'Calibration\LoginController@getLogin')->name('get-login');
    Route::POST('login', 'Calibration\LoginController@postLogin')->name('post-login');
});

// route logout
Route::GET('logout','Calibration\LoginController@getLogout')->name('keluar.logout')->middleware('auth');
